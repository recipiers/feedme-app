# feedme-app

FeedMe client app

## Documentation
To see all the documentation document, download the project and execute styleguide/index.html

## CSS Component
- Every component has it's own scss document, so every new component should have his own scss.
- To avoid CSS collisions, it's recommended use CSS modules.
    * To configure that we only have to change the name of the scss file. Eg: `style.scss` to `style.module.scss`.
    * Import css file to the component like this -> Eg. 
        ```
        import style from './style.module.css'
        ```
    * To finaly make module works, we need to add css classNames like that:

        ```
        <div className={style.classStyle}> </div>
        ```
        Or if we want combine CSS Module with Materialize classes:
        ```
        <div className={`col s12 ${style.classStyle}`}> </div>
        ```