import React, { Component } from 'react'
import { NavLink, Redirect } from 'react-router-dom';
import AuthService from '../../../services/auth.service';
import { isEmail } from "validator";

//Imports
import Logo from '../../../assets/images/logo_feed_me.png';
import 'materialize-css/dist/css/materialize.min.css';
import style from './login.module.css';
import M from 'materialize-css';

/**
 * User login component.
 *
 * @version 1.0.0
 * @author [Carlos Esteban](https://gitlab.com/iaw47999217)
 */
export default class Login extends Component {

    constructor(props){
        super(props);
        this.state = {
            userEmail: "",
            password: "",
            loading: false,
            message: "",
            redirect:false
        }
    }

    /**
     * Detects a change on the email input and actualizes the state
     * @param e Event
     * @public
     */
    onChangeUserEmail = (e)=>{
        let emailInput = document.querySelector('input[type="email"]');
        if (e.target.value.length === 0) {
            emailInput.classList.add('invalid')
        } else {
            emailInput.classList.remove('invalid');
        }

        this.setState({
            userEmail: e.target.value
        });
    }

    /**
     * Detects a change on the password input and actualizes the state
     * @param e Event
     * @public
     */
    onChangePassword = (e) => {
        let passwordInput = document.querySelector('input[type="password"]');
        if (e.target.value.length === 0) {
            passwordInput.classList.add('invalid')
        } else {
            passwordInput.classList.remove('invalid');
        }

        this.setState({
            password: e.target.value
        });
    }

    /**
     * Makes the call to login service
     * @param e Event
     * @public
     */
    handleLogin =(e) =>{
        e.preventDefault();

        if (this.validateForm()) {
            this.setState({
                message: "",
                loading: true
            });

            AuthService.login(this.state.userEmail, this.state.password).then( 
                () => {
                    this.setState({
                        redirect:true
                    });
                },
                error => {
                    const resMessage = 
                        (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                        error.message ||
                        error.toString();
                    this.setState({
                        loading: false,
                        message: resMessage
                    });
                    M.toast({
                        html:`${resMessage}`,
                        classes:'rounded errorToast'
                    })
                });
        } else {
            this.setState({
                loading: false
            });
        }
    }

    /**
     * Validates the form
     * @param
     * @public
     */
    validateForm = () => {
        if (!isEmail(this.state.userEmail)) {
            M.toast({
                html:`El email no es válido`,
                classes:'rounded errorToast'
            })
            let emailInput = document.querySelector('input[type="email"]');
            emailInput.classList.add('invalid')
            return false;
        } else if (this.state.password.length === 0 || this.state.password === undefined )  {
            M.toast({
                html:`La contraseña no puede estar vacía`,
                classes:'rounded errorToast'
            })
            let emailInput = document.querySelector('input[type="password"]');
            emailInput.classList.add('invalid');
            return false;
        }
        else {
            return true;
        }
    }

    render() {

        if (this.state.redirect) {
            return(
                <Redirect to={'/buscar'} />
            );
        }

        return (
            <main className={style.background}>
                <div className={style.fullHeight}>
                    <div className={`${style.mg0} row`}>
                        <div className={`col s12 l6 ${style.logoLayer} center-align hide-on-med-and-down`}>
                            <img className={`responsive-img center-align ${style.logo}`} src={Logo} alt="Logo" />
                        </div>
                        <div className={`col s12 l6 ${style.login}`}>
                            <div className={`${style.mg0} row`}>
                                <div className="col push-s1 s10 push-l2 l8">
                                    <form onSubmit={this.handleLogin}>
                                        <div className={`${style.mg0} row`}>
                                            <div className="col s12 show-on-medium-and-down hide-on-large-only center-align">
                                                <img className={`responsive-img center-align ${style.logo}`} src={Logo} alt="Logo" />
                                            </div>
                                            <div className="col s12 hide-on-small-only">
                                                <h1 className={style.welcome}>Bienvendo a FEEDME</h1>
                                            </div>
                                            <div className="input-field col s12">
                                                <i className="material-icons prefix">mail_outline</i>
                                                <input 
                                                    id="email" 
                                                    type="email" 
                                                    name="email" 
                                                    className=""
                                                    value={this.state.userEmail}
                                                    onChange={this.onChangeUserEmail}
                                                    required 
                                                />
                                                <label htmlFor="email">Email</label>
                                            </div>
                                        </div>
                                        <div className={`${style.mg0} row`}>
                                            <div className="input-field col s12">
                                                <i className="material-icons prefix">lock_outline</i>
                                                <input 
                                                    id="password" 
                                                    type="password" 
                                                    email="email" 
                                                    className=""
                                                    onChange={this.onChangePassword}
                                                    required
                                                />
                                                <label htmlFor="password">Contraseña</label>
                                            </div>
                                        </div>
                                        <div className={`${style.mg0} row`}>
                                            <div className="input-field col s12">
                                                <button className="col push-s1 s11 btn waves-effect waves-light" type="submit" name="action">Iniciar Sesión</button>
                                            </div>
                                        </div>
                                        <div className={`${style.mg0} row`}>
                                            <div className="input-field col s12">
                                                <p className="col push-s1 s11">Todavía no te has registrado? <NavLink to="/registro">Regístrate</NavLink> </p>
                                            </div>
                                        </div>
                                        {
                                            this.state.loading &&
                                            (
                                                <div className={`${style.mg0} center row`}>
                                                    <div className="preloader-wrapper big active ">
                                                        <div className="spinner-layer spinner-blue ">
                                                            <div className="circle-clipper left">
                                                            <div className="circle"></div>
                                                            </div><div className="gap-patch">
                                                            <div className="circle"></div>
                                                            </div><div className="circle-clipper right">
                                                            <div className="circle"></div>
                                                            </div>
                                                        </div>
                                                        <div className="spinner-layer spinner-red">
                                                            <div className="circle-clipper left">
                                                            <div className="circle"></div>
                                                            </div><div className="gap-patch">
                                                            <div className="circle"></div>
                                                            </div><div className="circle-clipper right">
                                                            <div className="circle"></div>
                                                            </div>
                                                        </div>
                                                        <div className="spinner-layer spinner-yellow">
                                                            <div className="circle-clipper left">
                                                            <div className="circle"></div>
                                                            </div><div className="gap-patch">
                                                            <div className="circle"></div>
                                                            </div><div className="circle-clipper right">
                                                            <div className="circle"></div>
                                                            </div>
                                                        </div>
                                                        <div className="spinner-layer spinner-green">
                                                            <div className="circle-clipper left">
                                                            <div className="circle"></div>
                                                            </div><div className="gap-patch">
                                                            <div className="circle"></div>
                                                            </div><div className="circle-clipper right">
                                                            <div className="circle"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                )
                                            }
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}