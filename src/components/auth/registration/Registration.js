import React, { Component } from "react";
import { NavLink } from 'react-router-dom';
import { isEmail } from "validator";
import AuthService from "../../../services/auth.service";

//Imports
import Logo from '../../../assets/images/logo_feed_me.png';
import 'materialize-css/dist/css/materialize.min.css';
import style from './registration.module.css';
import M from 'materialize-css';


/**
 * User registration component.
 *
 * @version 1.0.0
 * @author [Carlos Esteban](https://gitlab.com/iaw47999217)
 */
export default class Registration extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userName: "",
            email: "",
            password: "",
            password_confirmation: "",
            registrationErrors: "",
            loading: false,
            message: "",
            success: false
        };
    }

    /**
     * Submits the registration form
     * @param event Event
     * @public
     */
    handleSubmit = async(event) => {
        event.preventDefault();

        const {
            userName,
            email,
            password,
            password_confirmation
        } = this.state;

        if (this.validateForm()) {
            this.setState({
                message: "",
                loading: true
            })

            await AuthService.register(userName, email, password, password_confirmation)
            .then(  response => {
                    this.setState({
                        message: response.data.message,
                        success: true
                    }, async() =>{
                        await AuthService.login(email, password)
                        .then( response => {
                            this.props.history.push("/busqueda-primaria");
                            window.location.reload();
                        })
                        .catch(error => {
                            const resMessage = 
                                (error.response &&
                                error.response.data &&
                                error.response.data.message) ||
                                error.message ||
                                error.toString();
                            M.toast({
                                html:`El registro se ha completado correctamente, pero el login no se ha comlpetado: ${resMessage}`,
                                classes:'rounded errorToast',
                                displayLength: 10000
                            })
                            this.setState({
                                message: resMessage,
                                loading: false
                            })
                        })
                    });
            })
            .catch(error => {
                const resMessage = 
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();
                this.setState({
                    successful: false,
                    loading: false,
                    message: resMessage
                });
                M.toast({
                    html:`No se ha podido completar el registro. Intentalo más tarde. ${resMessage}`,
                    classes:'rounded errorToast'
                });
            });
        } else {
            this.setState({
                loading: false
            })
        }
    };

    /**
     * Validates the register form
     * @param
     * @public
     */
    validateForm = () => {
        if (this.state.userName.length === 0) {
            M.toast({
                html:`El nombre es obligatorio`,
                classes:'rounded errorToast'
            })
            let nameInput = document.querySelector('input[type="text"');
            nameInput.classList.add('invalid');
            return false;
        } else if (!isEmail(this.state.email)) {
            M.toast({
                html:`El email no es válido`,
                classes:'rounded errorToast'
            })
            let emailInput = document.querySelector('input[type="email"]');
            emailInput.classList.add('invalid')
            return false;
        } else if (this.state.password.length <= 7) {
            M.toast({
                html:`La contraseña debe tener al menos 8 caracteres.`,
                classes:'rounded errorToast'
            });
            let passwordInput = document.querySelector('#password');
            let passwordConfirmInput = document.querySelector('#confirm_password');
            passwordInput.classList.add('invalid');
            passwordConfirmInput.classList.add('invalid');
        } else if (this.state.password !== this.state.password_confirmation) {
            M.toast({
                html:`Confirma la contraseña correctamente.`,
                classes:'rounded errorToast'
            });
            let passwordInput = document.querySelector('#password');
            let passwordConfirmInput = document.querySelector('#confirm_password');
            passwordInput.classList.add('invalid');
            passwordConfirmInput.classList.add('invalid');
            return false;
        } else{
            return true;
        }
    }

    /**
     * Detects a change on the name input and actualizes the state
     * @param e Event
     * @public
     */
    onChangeName = (e) => {
        let nameInput = document.querySelector('input[type="text"]');
        if (e.target.value.length === 0) {
            nameInput.classList.add('invalid');
        } else {
            nameInput.classList.remove('invalid');
        }

        this.setState({
            userName: e.target.value
        });
    }

    /**
     * Detects a change on the email input and actualizes the state
     * @param e Event
     * @public
     */
    onChangeEmail = (e) => {
        let emailInput = document.querySelector('input[type="email"]');
        if (e.target.value.length === 0) {
            emailInput.classList.add('invalid')
        } else {
            emailInput.classList.remove('invalid');
        }

        this.setState({
            email: e.target.value
        });
    }

    /**
     * Detects a change on the password input and actualizes the state
     * @param e Event
     * @public
     */
    onChangePassword = (e) => {
        let passwordInput = document.querySelector('#password');
        if (e.target.value.length < 8) {
            passwordInput.classList.add('invalid');
        } else {
            passwordInput.classList.remove('invalid');
        }

        this.setState({
            password: e.target.value
        })
    }

    /**
     * Detects a change on the confirm password input and actualizes the state
     * @param e Event
     * @public
     */
    onChangePasswordConfirm = (e) => {
        let passwordInput = document.querySelector('#password');
        let passwordConfirmInput = document.querySelector('#confirm_password');
        if (e.target.value !== this.state.password || this.state.password.length < 8) {
            passwordInput.classList.remove('valid');
            passwordInput.classList.add('invalid');
            passwordConfirmInput.classList.remove('valid');
            passwordConfirmInput.classList.add('invalid');
        } else {
            passwordInput.classList.remove('invalid');
            passwordInput.classList.add('valid');
            passwordConfirmInput.classList.remove('invalid');
            passwordConfirmInput.classList.add('valid');
        }

        this.setState({
            password_confirmation: e.target.value
        })
    }


    render() {
        return (
        <main className={style.background}>
            <div className={style.fullHeight}>
                <div className={`${style.mg0} row`}>
                    <div className={`col s12 l6 ${style.logoLayer} center-align hide-on-med-and-down`}>
                        <img className={`responsive-img center-align ${style.logo}`} src={Logo} alt="Logo" />
                    </div>
                    <div className={`col s12 l6 ${style.login}`}>
                        <div className={`${style.mg0} row`}>
                            <div className="col push-s1 s10 push-l2 l8">
                                <form onSubmit={this.handleSubmit}>
                                    <div className={`${style.mg0} row`}>
                                        <div className="col s12 show-on-medium-and-down hide-on-large-only center-align">
                                            <img className={`responsive-img center-align ${style.logo}`} src={Logo} alt="Logo" />
                                        </div>
                                        <div className="col s12">
                                            <h1 className={style.welcome}>Crear cuenta</h1>
                                        </div>
                                    </div>
                                    <div className={`${style.mg0} row`}>
                                        <div className="input-field col s12">
                                            <i className="material-icons prefix">person_outline</i>
                                            <input 
                                                id="name" 
                                                type="text" 
                                                name="userName" 
                                                className=""
                                                value={this.state.userName}
                                                onChange={this.onChangeName}
                                            />
                                            <label htmlFor="name">Nombre</label>
                                        </div>
                                    </div>
                                    <div className={`${style.mg0} row`}>
                                        <div className="input-field col s12">
                                            <i className="material-icons prefix">mail_outline</i>
                                            <input 
                                                id="email" 
                                                type="email" 
                                                name="email" 
                                                className="" 
                                                value={this.state.email}
                                                onChange={this.onChangeEmail}
                                            />
                                            <label htmlFor="email">Email</label>
                                        </div>
                                    </div>
                                    <div className={`${style.mg0} row`}>
                                        <div className="input-field col s12">
                                            <i className="material-icons prefix">lock_open</i>
                                            <input 
                                                id="password" 
                                                type="password" 
                                                name="password" 
                                                className=""
                                                
                                                onChange={this.onChangePassword}
                                            />
                                            <label htmlFor="password">Contraseña</label>
                                        </div>
                                    </div>
                                    <div className={`${style.mg0} row`}>
                                        <div className="input-field col s12">
                                            <i className="material-icons prefix">lock_outline</i>
                                            <input 
                                                id="confirm_password" 
                                                type="password" 
                                                name="password_confirmation" 
                                                className=""
                                                
                                                onChange={this.onChangePasswordConfirm}
                                            />
                                            <label htmlFor="confirm_password">Confirma la contraseña</label>
                                        </div>
                                    </div>
                                    <div className={`${style.mg0} row`}>
                                        <div className="input-field col s12">
                                            <button className="col push-s1 s11 btn waves-effect waves-light" type="submit" name="action">Registrarme</button>
                                        </div>
                                    </div>
                                    <div className={`${style.mg0} row`}>
                                        <div className="input-field col s12">
                                            <p className="col push-s1 s11">Ya tienes una cuenta? <NavLink to="/login">Inicia sesión</NavLink> </p>
                                        </div>
                                    </div>
                                    {
                                        this.state.loading &&
                                        (
                                            <div className={`${style.mg0} center row`}>
                                                <div className="preloader-wrapper big active ">
                                                    <div className="spinner-layer spinner-blue ">
                                                        <div className="circle-clipper left">
                                                        <div className="circle"></div>
                                                        </div><div className="gap-patch">
                                                        <div className="circle"></div>
                                                        </div><div className="circle-clipper right">
                                                        <div className="circle"></div>
                                                        </div>
                                                    </div>
                                                    <div className="spinner-layer spinner-red">
                                                        <div className="circle-clipper left">
                                                        <div className="circle"></div>
                                                        </div><div className="gap-patch">
                                                        <div className="circle"></div>
                                                        </div><div className="circle-clipper right">
                                                        <div className="circle"></div>
                                                        </div>
                                                    </div>
                                                    <div className="spinner-layer spinner-yellow">
                                                        <div className="circle-clipper left">
                                                        <div className="circle"></div>
                                                        </div><div className="gap-patch">
                                                        <div className="circle"></div>
                                                        </div><div className="circle-clipper right">
                                                        <div className="circle"></div>
                                                        </div>
                                                    </div>
                                                    <div className="spinner-layer spinner-green">
                                                        <div className="circle-clipper left">
                                                        <div className="circle"></div>
                                                        </div><div className="gap-patch">
                                                        <div className="circle"></div>
                                                        </div><div className="circle-clipper right">
                                                        <div className="circle"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            )
                                        }
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        );
    }
}
