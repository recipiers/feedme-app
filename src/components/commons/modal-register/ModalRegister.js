import React, { Component } from 'react';
import M from 'materialize-css';
import 'materialize-css';
import LogoMobile from '../../../assets/images/logo_feed_me_mobile.png';
import { NavLink } from 'react-router-dom';
import style from './modal-register.module.css';

/**
 * Modal that advice the user he/she is not allowed to use a function, and invites the user to register.
 *
 * @version 1.0.0
 * @author [Carlos Esteban](https://gitlab.com/iaw47999217)
 */
export default class ModalRegister extends Component {

    componentDidMount(){
        this.modalIni();
    }

    /**
     * Initializes the modal
     * @param
     * @public
     */
    modalIni = () => {
        var options = {};
        var elems = document.querySelectorAll('.modal');
        M.Modal.init(elems, options);
    }

    render() {
        return (
            <React.Fragment>
                {/*Modal Register */}
                <div id="register" className="modal center">
                    <div className="modal-content">
                        <img className="responsive-img " src={LogoMobile} alt="Logo"/>
                        <h4 className={style.titleModal}>¡Regístrate!</h4>
                        <p>Así podrás guardar tus búsquedas realizadas y tus recetas favoritas.</p>
                    </div>
                    <div className="modal-footer">
                        <NavLink to="/login" className="modal-close waves-effect waves-green btn-flat">Iniciar sesión</NavLink>
                        <NavLink to="/registro" className="modal-close waves-effect waves-green btn-flat">Registrarme</NavLink>
                    </div>
                </div>
            </React.Fragment>
            
        );
    }
}
