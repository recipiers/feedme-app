import React, { Component } from 'react';
import 'materialize-css';
import style from './receipts-grid.module.css';
import M from 'materialize-css';
import image from '../../../assets/images/home-background.jpg';
import { NavLink } from 'react-router-dom';
import authHeader from '../../../services/auth-header';
import UserService from '../../../services/user.service';

/**
 * Prints recipes grid
 *
 * @version 1.0.0
 * @author [Carlos Esteban](https://gitlab.com/iaw47999217)
 */
export default class ReceiptsGrid extends Component {

    constructor(props) {
        super(props);
        this.state = {
            receipts: [],
            isLoggedIn: authHeader()
        }
    }

    componentDidMount() {
        //recogemos la prop de las recetas
        let receipts = this.props.receipts;
        this.setState({
            receipts: receipts
        })
    }

    /**
     * Render the level of the receipt
     * @param number Number of difficulty
     * @public
     */
    renderDifficulty = (number) => {
        switch (number) {
            case 1:
                return(
                    <React.Fragment>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star_border</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star_border</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star_border</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star_border</i>
                    </React.Fragment>
                );
                break;
            case 2:
                return(
                    <React.Fragment>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star_border</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star_border</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star_border</i>
                    </React.Fragment>
                );
                break;
            case 3:
                return(
                    <React.Fragment>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star_border</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star_border</i>
                    </React.Fragment>
                );
                break;
            case 4:
                return(
                    <React.Fragment>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star_border</i>
                    </React.Fragment>
                );
                break;
            case 5:
                return(
                    <React.Fragment>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                        <i className={`material-icons ${style.iconsMobile}`}>star</i>
                    </React.Fragment>
                );
                break;
            default:
                break;
        }
    }

    /**
     * Function that adds a fav receipt 
     * 
     * @param id_receipt Id of the receipt
     * @public
     */
    saveFavReceipt = async(id_receipt) => {

        await UserService.toogleFavReceipt(id_receipt)
            .then(response => {
                let receipts = this.state.receipts;
                for (let i = 0; i < receipts.length; i++) {
                    if (receipts[i].id === id_receipt) {
                        receipts[i].favourite = true
                    }
                }
                this.setState({
                    receipts: receipts
                })
                M.toast({
                    html:`${response.data.message}`,
                    classes:'rounded succedToast'
                })
            })
            .catch(error => {
                M.toast({
                    html:`${error}`,
                    classes:'rounded errorToast'
                })
            })
    }

    /**
     * Function that undo a fav receipt
     * @param id_receipt Id of the receipt
     * @public
     */
    undoFavReceipt = async(id_receipt) => {
        await UserService.toogleFavReceipt(id_receipt)
            .then( response => {
                console.log(response);
                M.toast({
                    html:`${response.data.message}`,
                    classes:'rounded succedToast'
                })
            })
            .catch( error => {
                console.log(error);
                M.toast({
                    html:`${error}`,
                    classes:'rounded errorToast'
                })
            })
        let receipts = this.state.receipts;
        for (let i = 0; i < receipts.length; i++) {
            if (receipts[i].id === id_receipt) {
                receipts[i].favourite = false
            }
        }
        this.setState({
            receipts: receipts
        })
    }

    /**
     * This function renders the favButton depends if the user is loggedIn or not, or if it's a fav receipt
     * 
     * @param isFavourite Indicates if the receipt is a fav one
     * @param receiptId id of the receipt
     * @public
     */
    renderFavButton = (isFavourite, receiptId) => {
        
        if (this.state.isLoggedIn !== null) {
            if (isFavourite) {
                return (
                    <a href="#!" onClick={() => this.undoFavReceipt(receiptId)}><i className={`material-icons ${style.favIcon}`}>favorite</i></a>
                );
            } else{
                return (
                    <a href="#!" onClick={() => this.saveFavReceipt(receiptId)}><i className={`material-icons ${style.favIcon}`}>favorite_border</i></a>
                );
            }
        } else {
            return(
                <a href="#!"><i className={`material-icons ${style.favIcon}`}>favorite_border</i></a>
            );
        }
    }

    render() {
            let listReceipts = this.state.receipts.map((receipt, i) => {
                return(
                    <div key={i} className="col s12 m6 l4">
                        <div className="card">
                            <div className={style.recipeImageContainer}>
                            {/*Si la receta no tiene imagen añadimos una por defecto*/ 
                                receipt.pictureurl !== null ? (
                                    <img className="responsive-img" src={receipt.pictureurl} alt={receipt.name} />
                                ) : (
                                    <img className="responsive-img" src={image} alt={receipt.name} />
                                )
                            }
                            </div>
                            <div className={style.contentCard}>
                                <div className={`row ${style.mb0}`}>
                                    <div className={`col s12 left-align ${style.titleCardDiv}`}>
                                        <span className={`${style.titleCard}  truncate`}>{receipt.name}</span>
                                    </div>
                                    <div className={`col s12 left-align ${style.timeCardDiv}`}>
                                        <span className={style.timeCard}><i className="tiny material-icons">access_time</i>{receipt.time}'</span>
                                    </div>
                                    <div className="col s12">
                                        <div className={`row ${style.mb0}`}>
                                            <div className="col s2">
                                                {this.renderFavButton(receipt.favourite, receipt.id)}
                                            </div>
                                            <div className="col s6 right-align">
                                                {this.renderDifficulty(receipt.difficulty)}
                                            </div>
                                            <div className="col s4 right-align">
                                                <NavLink to={'/receta/'+receipt.id}>VER RECETA</NavLink>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            });
            
            return (
                <div className={`row ${style.mb0}`}>
                    {listReceipts}
                </div>
            )
    }
}
