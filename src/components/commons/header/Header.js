import React, { Component } from 'react';
import M from 'materialize-css';
import HorizontalLogo from '../../../assets/images/logo_horizontal.png';
import HorizontalLogoMobile from '../../../assets/images/logo_horizontal_mobile.png';
import 'materialize-css';
import style from './header.module.css';
import { NavLink } from 'react-router-dom';
import authHeader from '../../../services/auth-header';
import userService from '../../../services/user.service';
import authService from '../../../services/auth.service';

/**
 * Component to render menu.
 *
 * @version 1.0.0
 * @author [Carlos Esteban](https://gitlab.com/iaw47999217)
 */
export default class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: authHeader(),
            user: {}
        }
    }

    componentDidMount(){
        this.getUserData();
        this.sidenavInit();
        this.dropdowns();
    }

    /**
     * Gets the user data if the user is loggedIn
     * @param
     * @public
     */
    getUserData =  async() => {
        if (this.state.isLoggedIn !== null) {
            try {
                const response = await userService.getUserProfileData();
                this.setState({
                    user: response.data.user
                })
            } catch (error) {
                M.toast({
                    html:`No es posible recibir datos de tu usuario: ${error}`,
                    classes:'rounded errorToast',
                    displayLength: 10000
                })
            }
            
        }
    }

    /**
     * This function initialize sidenav on mobile and tablet devices
     * @param
     * @public
     */
    sidenavInit = () => {
        let elems = document.querySelectorAll('.sidenav');
        M.Sidenav.init(elems);
        //$(".dropdown-trigger").dropdown();
    }

    /**
     * Initializes the droptdown menu if the user is loggedIn
     * @param
     * @public
     */
    dropdowns = () => {
        if (this.state.isLoggedIn !== null) {
            let dropdown = document.querySelector('.dropdown-trigger');
            M.Dropdown.init(dropdown);
        }
    }

    render() {
        
        return (
            <header>
                {/*Dropdown Structure */}
                <ul id="dropdown" className="dropdown-content">
                    <li>
                        <NavLink to="/buscar">Buscar</NavLink>
                    </li>
                    <li className="divider"></li>
                    <li>
                        <NavLink to="/perfil#recetas-favoritas">Recetas favoritas</NavLink>
                    </li>
                    <li className="divider"></li>
                    <li>
                        <NavLink to="/perfil">Mi perfil</NavLink>
                    </li>
                    <li className="divider"></li>
                    <li>
                        <a href="#!" onClick={()=>{authService.logout()}}>Cerrar sesión</a>
                    </li>
                </ul>
                <nav>
                    <div className="nav-wrapper">
                        <button data-target="mobile-demo" className="btn navMobileBtn sidenav-trigger hide-on-large-only"><i className="material-icons ">menu</i></button>
                        <NavLink to="/buscar" className="brand-logo hide-on-large-only"><img className="responsive-img"  src={HorizontalLogoMobile} alt="Logo"/></NavLink>
                        <ul className="left hide-on-med-and-down">
                            <li><NavLink to="/buscar"><img className="responsive-img left" src={HorizontalLogoMobile} alt="Logo"/></NavLink></li>
                            <li><NavLink to="/buscar" className={style.searchBtn}><i className="material-icons left">search</i>Buscar</NavLink></li>
                        </ul>
                            {
                                this.state.isLoggedIn !== null ? (
                                    <ul className="right hide-on-med-and-down">
                                        <li>
                                            <a
                                                className="dropdown-trigger"
                                                href="#!"
                                                data-target="dropdown"
                                            >
                                                <i className="material-icons right">person</i>{this.state.user.name}
                                            </a>
                                        </li>
                                    </ul>
                                ) : (
                                <ul className="right hide-on-med-and-down">
                                    <li><NavLink to="/login" >Iniciar Sesión</NavLink></li>
                                    <li><NavLink to="/registro" className={`waves-effect waves-light btn ${style.registerBtn}`}>Regístrate</NavLink></li>
                                </ul>
                                )
                            }
                    </div>
                </nav>
                <ul className="sidenav" id="mobile-demo">
                    <li>
                        <div className="user-view ">
                            <div className="logo">
                                <img className="responsive-img" src={HorizontalLogo} alt="Logo" />
                            </div>
                            
                        </div>
                    </li>
                    {
                        this.state.isLoggedIn !== null ? (
                            <React.Fragment>
                                <li><NavLink to="/buscar" onClick={this.sidenavInit} className={style.searchBtn}><i className="material-icons">search</i>Buscar</NavLink></li>
                                <li><div className="divider"></div></li>
                                <li><NavLink to="/perfil#recetas-favoritas" onClick={this.sidenavInit} className={style.searchBtn}><i className="material-icons">favorite</i>Recetas favoritas</NavLink></li>
                                <li><div className="divider"></div></li>
                                <li><NavLink to="/perfil" onClick={this.sidenavInit} className={style.searchBtn}><i className="material-icons">person</i>Mi perfil</NavLink></li>
                                <li><div className="divider"></div></li>
                                <li><a href="#!" onClick={()=>{authService.logout()}} className={style.searchBtn}><i className="material-icons">settings_power</i>Cerrar sesión</a></li>
                            </React.Fragment>
                        ) : (
                            <React.Fragment>
                                <li><NavLink to="/buscar" onClick={this.sidenavInit} className={style.searchBtn}><i className="material-icons">search</i>Buscar</NavLink></li>
                                <li><div className="divider"></div></li>
                                <li><NavLink to="/login" onClick={this.sidenavInit} className="center">Iniciar Sesión</NavLink></li>
                                <li><NavLink to="/registro" onClick={this.sidenavInit}  className={`btn waves-effect waves-light center ${style.registerBtn}`}>Regístrate</NavLink></li>
                            </React.Fragment>
                        )
                    }
                </ul>
            </header>
        )
    }
}
