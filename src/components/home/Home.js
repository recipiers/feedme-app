import React, { Component } from 'react'
import { NavLink, Redirect } from 'react-router-dom'

//Image and css imports
import Logo from '../../assets/images/logo_feed_me.png';
import 'materialize-css/dist/css/materialize.min.css';
import style from'./home.module.css';
import authHeader from '../../services/auth-header';


/**
 * Home component.
 *
 * @version 1.0.0
 * @author [Carlos Esteban](https://gitlab.com/iaw47999217)
 */
export default class Home extends Component {

    constructor(props){
        super(props);
        this.state = {
            isLoggedIn: authHeader(),
            redirect: false
        }
    }

    componentDidMount(){
        //If user is logged in, is redirected to search form
        if (this.state.isLoggedIn) {
            this.setState({
                redirect: true
            })
        }
    }

    render() {
        if (this.state.isLoggedIn) {
            return(
                <Redirect to={'/buscar'} />
            );
        }

        return (
            <main className={style.background}>
                <div className={style.fullHeight}>
                    <div className="row">
                        <div className={`col s12 center-align ${style.logo}`}>
                            <img
                                className="responsive-img"
                                src={Logo}
                                alt="Logo"
                            />
                        </div>
                        <div className="col s6">
                            <NavLink to="/buscar" className={`waves-effect waves-light btn right ${style.searchButton}`}>
                                Buscar recetas
                            </NavLink>
                        </div>
                        <div className="col s6">
                            <NavLink to="/login" className={`waves-effect waves-light btn left ${style.loginButton}`}>
                                Iniciar sesión
                            </NavLink>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}
