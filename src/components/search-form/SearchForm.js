import React, { Component } from 'react';
import M from 'materialize-css';
import 'materialize-css';
import Logo from '../../assets/images/logo_feed_me.png';
import LogoMobile from '../../assets/images/logo_feed_me_mobile.png';
import style from './search-form.module.css';
import { Redirect } from 'react-router-dom';
import ModalRegister from '../commons/modal-register/ModalRegister';

//Services imports
import { getIngredients,getAllergens } from '../../services/FormSearchService';
import UserService from '../../services/user.service';
import authHeader from '../../services/auth-header';
import userService from '../../services/user.service';
import { apiResponseToChipsFormat, chipsFormatToApiRequest, getIdFromApiFormatObj } from '../../services/globals';

/**
 * Principal component of the aplication. Contains a form to make recipe searches
 *
 * @version 1.0.0
 * @author [Carlos Esteban](https://gitlab.com/iaw47999217)
 */
export default class SearchForm extends Component {

    constructor(props){
        super(props);
        this.state = {
            haveThisIngredients: [],
            doNotHaveTheseIngredients: [],
            ingredientsApiFormat: [],
            ingredientsAutocomplete: {},
            isvegetarian: false,
            isvegan: false,
            radioBtnChecked: false,
            allergens:[],
            allergensApiFormat: [],
            allergensAutocomplete: {},
            minutes: '0',
            redirect: false,
            search: {},
            primarySearch: {},
            savedSearches: [],
            isLoggedIn: authHeader()
        }
    }

    componentDidMount(){                
        this.modalsIni();
        this.getDataAutocompleteLists();
        this.getSavedSearches();
    }

    /**
     * Get the saved searches if the user is loggedIn
     * @param
     * @public
     */
    getSavedSearches = async() => {
        if (this.state.isLoggedIn !== null) {
            let searches = await UserService.getSavedSearches();
            this.setState({
                savedSearches: searches
            })
        }
    }

    /**
     * Renders saved searches list on saved-searches modal
     * @param
     * @public
     */
    renderSavedSearches = () => {
        if (this.state.savedSearches.length > 0) {
            let searches = this.state.savedSearches.map((item, i) => {
                return (
                    <li key={i} className="collection-item"><div>{item.title || "Sin título"}<a href="#!" onClick={() => {this.doSavedSearch(item.id)}} className="secondary-content"><i className={`material-icons ${style.brandColor}`}>search</i></a></div></li>
                );
            });
            return searches;
        } else {
            return(
                <p className="center">No hay búsquedas guardadas.</p>
            );
        }
    }

    /**
     * Function that makes the redirection and passes the search object
     * @param searchToDo Search id
     * @public
     */
    doSavedSearch = async(searchId) => {
        await userService.getStoredSearch(searchId)
            .then( storedSearch => {
                let searchObj = {
                    excludedIngredients: getIdFromApiFormatObj(storedSearch.excludedIngredients),
                    includedIngredients: getIdFromApiFormatObj(storedSearch.includedIngredients),
                    allergens: getIdFromApiFormatObj(storedSearch.allergensOrig),
                    isvegan: storedSearch.isvegan,
                    isvegetarian: storedSearch.isvegetarian,
                    maxtime: storedSearch.maxtime,
                    tags: []
                }
                this.setState({
                    search: searchObj,
                    redirect: true
                })
            })
            .catch(error => {
                M.toast({
                    html:`${error}`,
                    classes:'rounded errorToast'
                })
            })
    }

    /**
     * Function that provides lists of data from FormSearchService to use on autocomplete lists
     * @param
     * @public
     */
    getDataAutocompleteLists = async() => {
        await getIngredients()
            .then(ingredients => {
                let ingredientChipsFormatObj = apiResponseToChipsFormat(ingredients);
                this.setState({
                    ingredientsApiFormat: ingredients,
                    ingredientsAutocomplete: ingredientChipsFormatObj
                })
            })
            .catch(error => {
                M.toast({
                    html:`${error}`,
                    classes:'rounded errorToast'
                })
            })
        
        await getAllergens()
            .then(allergens => {
                let allergensChipsFormatObj = apiResponseToChipsFormat(allergens);
                this.setState({
                    allergensApiFormat: allergens,
                    allergensAutocomplete: allergensChipsFormatObj
                });
            })
            .catch(error => {
                M.toast({
                    html:`${error}`,
                    classes:'rounded errorToast'
                })
            })

        if (this.state.isLoggedIn !== null) {
            await userService.getPrimarySearch()
                .then( primarySearch => {
                    this.setState({
                        primarySearch:primarySearch,
                        doNotHaveTheseIngredients: primarySearch.doNotHaveTheseIngredients,
                        isvegetarian: primarySearch.isvegetarian,
                        isvegan: primarySearch.isvegan,
                        allergens: primarySearch.allergens
                    }, () => {
                        if (this.state.isvegetarian) {
                            document.searchReceipts.dietType[0].checked = true
                            this.setState({
                                radioBtnChecked:true
                            })
                        } else if (this.state.isvegan) {
                            document.searchReceipts.dietType[1].checked = true
                            this.setState({
                                radioBtnChecked:true
                            })
                        } else if (!this.state.isvegetarian && !this.state.isvegan) {
                            document.searchReceipts.dietType[2].checked = true
                            this.setState({
                                radioBtnChecked:true
                            })
                        }
                    })
                })
                .catch(error => {
                    M.toast({
                        html:`${error}`,
                        classes:'rounded errorToast'
                    })
                })
        } else {
            //If the user isn't connected, normal diet is checked
            document.searchReceipts.dietType[2].checked = true;
            this.setState({
                radioBtnChecked:true
            })
        }
        //Once the data is recived, initialize chips 
        this.chipsHaveIngredientsIni();
        this.chipsDoNotHaveIngredientsIni();
        this.chipsAllergensIni();
    }

    /**
     * Evaluates if radio buttons are selected
     * @param
     * @public
     */
    evalDiet = () => {
        let dietRadioForm = document.searchReceipts.dietType;

        //Si la dieta  esta seleccionada
        if (dietRadioForm.value === "on") {
            //cambiamos el estado
            this.setState({
                radioBtnChecked: true
            })
            //Quitamos el aviso al usuario
            let label = document.querySelector('#dietLabel');
            label.classList.remove('invalid');
        } else{
            //Mantenemos el state en falso
            this.setState({
                radioBtnChecked: false
            })
        }
    }

    /**
     * Function that changes the vegetarian diet state
     * @public
     */
    handleVegetarianDietChange = () => {
        this.setState({
            isvegetarian: true,
            isvegan: false
        })
        this.evalDiet();
    }

    /**
     * Function that changes the vegan diet state
     * @public
     */
    handleVeganDietChange = () => {
        this.setState({
            isvegetarian: false,
            isvegan: true
        })
        this.evalDiet();
    }

    /**
     * Function that changes the diet state
     * @public
     */
    handleAnyTypeOfDietChange = () => {
        this.setState({
            isvegetarian: false,
            isvegan: false
        })
        this.evalDiet();
    }

    /**
     * Function that changes the minutes in the state.
     * @param event
     * @public
     */
    handleMinuteChange = (event) => {
        //Cambiamos el estado
        this.setState({
            minutes: event.target.value
        });
    }

    /**
     * Submits the form and redirects the component
     * @param event
     * @public
     */
    handleSubmit = (event) => {
        event.preventDefault();

        const  {
            haveThisIngredients,
            doNotHaveTheseIngredients,
            isvegetarian,
            isvegan,
            radioBtnChecked,
            allergens,
            minutes
        } = this.state;
        
        if (!radioBtnChecked) {
            //Si el radiobutton no esta marcado mostramos un error al usuario y cambiamos el color del label
            let label = document.querySelector('#dietLabel');
            label.classList.add('invalid');
            M.toast({
                html:`Tienes que añadir el tipo de dieta para buscar recetas`,
                classes:'rounded errorToast'
            })
        } 
        else{
            //Objeto que servirá para hacer la request
            let search = {
                ingredients: haveThisIngredients,
                withoutTheseIngredients: doNotHaveTheseIngredients,
                isvegetarian: isvegetarian,
                isvegan: isvegan,
                allergens: allergens,
                minutes: minutes 
            }

            let transformedSearch = chipsFormatToApiRequest(search, this.state.ingredientsApiFormat, this.state.allergensApiFormat);

            //Cambiamos el state para permitir la redirección
            this.setState({
                redirect: true,
                search: transformedSearch
            })
        }
    }

    /**
     * This function initializes all the modals of this component
     * @public
     */
    modalsIni = () => {
        var options = {};
        var elems = document.querySelectorAll('.modal');
        M.Modal.init(elems, options);
    }

    /**
     * Initializes chips on the haveIngredients input
     * @param
     * @public
     */
    chipsHaveIngredientsIni = () => {
        var elem = document.querySelector('.haveThisIngredients');
        
        var options = {
            //Añade ingredientes por defecto
            //data:[{tag:'Huevos'}]
            data:this.state.haveThisIngredients,
            //Ingredientes de la base de datos
            autocompleteOptions:{
                data: this.state.ingredientsAutocomplete
            },
            limit: 20,
            secondaryPlaceholder: "+ ingredientes",
            onChipAdd: function(e, chip){//Esta función actua como eventListener al añadir un ingrediente
                let item = chip.childNodes[0].textContent;
                //Comprueba si el ingrediente existe en la BD
                let existOnDB = this.state.ingredientsAutocomplete[item] !== null;
                let existOnOtherInput = false;
                //Comprueba si el ingrediente ya ha sido introducido en el otro input
                for (let i = 0; i < this.state.doNotHaveTheseIngredients.length; i++) {
                    if (this.state.doNotHaveTheseIngredients[i].tag === item) {
                        existOnOtherInput = true;
                        M.toast({
                            html:`El ingrediente ${item} ya ha sido seleccionado anteriormente`,
                            classes:'rounded errorToast'
                        })
                    }
                }

                //Obtenemos la instancia del objeto en el DOM
                let haveIngredients = document.querySelector('.haveThisIngredients');
                let instance = M.Chips.getInstance(haveIngredients);

                // Se comprueba si el ingrediente añadido existe en la lista de alimentos existente en la BD 
                // y tampoco existe en en input de ingredientes que no contenga la receta               
                if (existOnDB || existOnOtherInput) {
                    //En el caso de que no exista ese ingrediente, se elimina el ingrediente del input
                    //Eliminamos el ultimo ingrediente, que es el que se acaba de añadir
                    instance.deleteChip(instance.chipsData.length - 1);
                    
                } else{
                    //Si hay al menos 1 ingrediente se elimina el aviso al usuario
                    if (instance.chipsData.length > 0) {
                        let label = document.querySelector('#ingredientsLabel');
                        label.classList.remove('invalid');
                    }
                    //Si existe el ingrediente se añade al state
                    this.setState({
                        haveThisIngredients: e[0].M_Chips.chipsData
                    });
                }
            }.bind(this),
            onChipDelete: function(e, chip) {
            }
        };
        M.Chips.init(elem, options);    
    }
    
    /**
     * Initializes chips on the haveIngredients input
     * @param
     * @public
     */
    chipsDoNotHaveIngredientsIni = () => {
        var elems = document.querySelector('.doNotHaveTheseIngredients');
        var options = {
            //Añade ingredientes por defecto
            data:this.state.doNotHaveTheseIngredients,
            //Ingredientes de la base de datos
            autocompleteOptions:{
                data: this.state.ingredientsAutocomplete
            },
            limit: 20,
            secondaryPlaceholder: "+ ingredientes",
            onChipAdd: function(e, chip){//Esta función actua como eventListener al añadir un ingrediente
                var item = chip.childNodes[0].textContent;
                //Comprueba si el ingrediente existe en la BD
                let existOnDB = this.state.ingredientsAutocomplete[item] !== null;
                let existOnOtherInput = false;
                //Comprueba si el ingrediente ya ha sido introducido en el otro input
                for (let i = 0; i < this.state.haveThisIngredients.length; i++) {
                    if (this.state.haveThisIngredients[i].tag === item) {
                        existOnOtherInput = true;
                        M.toast({
                            html:`El ingrediente ${item} ya ha sido seleccionado anteriormente`,
                            classes:'rounded errorToast'
                        })
                    }
                }
                                
                //Se comprueba si el ingrediente añadido existe en la lista de alimentos existente en la BD
                // y tampoco existe en en input de ingredientes que contenga la receta                  
                if (existOnDB || existOnOtherInput) {
                    //En el caso de que no exista ese ingrediente, se elimina el ingrediente del input
                    let doNotHaveTheseIngredients = document.querySelector('.doNotHaveTheseIngredients');
                    let instance = M.Chips.getInstance(doNotHaveTheseIngredients);
                    //Eliminamos el ultimo ingrediente, que es el que se acaba de añadir
                    instance.deleteChip(instance.chipsData.length - 1);
                } else{
                    //Si existe el ingrediente se añade al state
                    this.setState({
                        doNotHaveTheseIngredients: e[0].M_Chips.chipsData
                    });
                }
            }.bind(this),
            onChipDelete: function(e, chip) {
            }
        };
        M.Chips.init(elems, options);
    }

    /**
     * Initializes chips on the allergies input
     * @param
     * @public
     */
    chipsAllergensIni = () => {
        var elems = document.querySelector('.allergies');
        var options = {
            //Añade ingredientes por defecto
            data:this.state.allergens,
            //Ingredientes de la base de datos
            autocompleteOptions:{
                data: this.state.allergensAutocomplete
            },
            limit: 20,
            secondaryPlaceholder: "+ alergias/intolerancias",
            onChipAdd: function(e, chip){//Esta función actua como eventListener al añadir un alérgeno
                var item = chip.childNodes[0].textContent;

                //Se comprueba si el alergeno añadido existe en la lista de alergenos de la BD
                if (this.state.allergensAutocomplete[item] !== null) {
                    //En el caso de que no exista ese alergeno, se elimina el alergeno del input
                    let allergens = document.querySelector('.allergies');
                    let instance = M.Chips.getInstance(allergens);
                    //Eliminamos el ultimo ingrediente, que es el que se acaba de añadir
                    instance.deleteChip(instance.chipsData.length - 1);
                } else {
                    //Si existe el alergeno se añade al state
                    this.setState({
                        allergens: e[0].M_Chips.chipsData
                    });
                }
            }.bind(this)
        };
        M.Chips.init(elems, options);
    }
    
    render() {
        //Si el state render está permitido (está en true) se realiza la redirección con el objeto de la búsqueda
        if (this.state.redirect) {
            return (
                <Redirect to={'/redirect/' + JSON.stringify(this.state.search)} />
            );
        } else {
            return (
                <main className={style.main}>
                    <div className="container">
                        <div className="row">
                            <div className="col s12 center-align hide-on-med-and-down show-on-large">
                                <img className="responsive-img" src={Logo} alt="Logo"/>
                            </div>
                            <div className="col s12 center-align hide-on-large-only show-on-medium-and-down">
                                <img className="responsive-img " src={LogoMobile} alt="Logo"/>
                            </div>
                            <div className={`col s12 center ${style.sloganDiv}`}>
                                <p className={style.slogan}>Nuevas ideas para degustar</p>
                            </div>
                            <div className={`col s12 center ${style.titleDiv}`}>
                                <h1 className={style.title}>¿Que buscas en una receta?</h1>
                                {
                                    this.state.isLoggedIn !== null ? (
                                        <a href="#saved-searches" className={`${style.savedSearch} modal-trigger`}>¿O prefieres usar una búsqueda guardada?</a>
                                    ) : (
                                        <a href="#register" className={`${style.savedSearch} modal-trigger`}>¿O prefieres usar una búsqueda guardada?</a>
                                    )
                                }
                                
                            </div>
                            <div className={`col s12 push-l2 l8 ${style.formDiv}`}>
                                <form 
                                    onSubmit={this.handleSubmit}
                                    name="searchReceipts"
                                >
                                    <div className="row">
                                        <div className="col s12">
                                            <div className="row">
                                                <div className="input-field col s12">
                                                    <i className="material-icons prefix">add_circle</i>
                                                    <div className={`chips chips-autocomplete haveThisIngredients ${style.mt40}`}></div>
                                                    <label id="ingredientsLabel" htmlFor="">Que contenga estos ingredientes</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col s12">
                                            <div className="row">
                                                <div className="input-field col s12">
                                                    <i className="material-icons prefix">remove_circle</i>
                                                    <div className={`chips chips-autocomplete doNotHaveTheseIngredients ${style.mt40}`}></div>
                                                    <label htmlFor="">Que no contenga estos ingredientes</label>
                                                </div>
                                            </div>
                                        </div>
                                        {/* Input dietas */}
                                        <div className="col s12">
                                            <div className="row">
                                                <div className="input-field col s12">
                                                    <i className="material-icons prefix">restaurant</i>
                                                    <div className={`col s6 push-s4 ${style.mt40}`}>
                                                        <p>
                                                            <label>
                                                                <input 
                                                                    className="with-gap" 
                                                                    name="dietType" 
                                                                    type="radio"
                                                                    onChange = {this.handleVegetarianDietChange}
                                                                />
                                                                <span>Vegetariana</span>
                                                            </label>
                                                        </p>
                                                        <p>
                                                            <label>
                                                                <input 
                                                                    className="with-gap" 
                                                                    name="dietType" 
                                                                    type="radio"
                                                                    onChange = {this.handleVeganDietChange} 
                                                                />
                                                                <span>Vegana</span>
                                                            </label>
                                                        </p>
                                                        <p>
                                                            <label>
                                                                <input 
                                                                    className="with-gap" 
                                                                    name="dietType" 
                                                                    type="radio" 
                                                                    onChange = {this.handleAnyTypeOfDietChange}
                                                                />
                                                                <span>Ninguna en especial</span>
                                                            </label>
                                                        </p>
                                                    </div>
                                                    <label id="dietLabel" htmlFor="">Que se adapte a mi dieta</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col s12">
                                            <div className="row">
                                                <div className="input-field col s12">
                                                    <i className="material-icons prefix">warning</i>
                                                    <div className={`chips chips-autocomplete allergies ${style.mt40}`}></div>
                                                    <label htmlFor="">Que respete mis alergias e intolerancias</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col s12">
                                            <div className="row">
                                                <div className="input-field col s12">
                                                    <i className="material-icons prefix">watch_later</i>
                                                    <div className={`col s11 push-s1 ${style.mt40}`}>
                                                        <div className={`col s12 hide-on-large-only ${style.timeRangeDivMobile}`}>
                                                            {this.state.minutes !== '0' &&
                                                                <p className={`center ${style.timeRange}`}>{this.state.minutes} minutos</p>
                                                            }
                                                            {this.state.minutes === '0' &&
                                                                <p className={`center ${style.timeRange}`}>Sin tiempo máximo</p>
                                                            }
                                                        </div>
                                                        <p className="range-field">
                                                            <input type="range" id="test5" name="minutes" min="0" max="200" value={this.state.minutes} onChange={this.handleMinuteChange} />
                                                        </p>
                                                    </div>
                                                    <div className={`col s12 hide-on-med-and-down ${style.timeRangeDiv}`}>
                                                        {this.state.minutes !== '0' &&
                                                            <p className={`center ${style.timeRange}`}>{this.state.minutes} minutos</p>
                                                        }
                                                        {this.state.minutes === '0' &&
                                                            <p className={`center ${style.timeRange}`}>Sin tiempo máximo</p>
                                                        }
                                                    </div>
                                                    <label htmlFor="minutes">Que se pueda preparar en...</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col s12">
                                        <div className="row">
                                            <div className="input-field col s12 center">
                                                <button
                                                    type="submit"
                                                    className={`waves-effect waves-light btn ${style.searchReceiptsBtn}`}
                                                >
                                                    Buscar recetas
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="#help" className={`btn-floating btn-large waves-effect waves-light modal-trigger ${style.helpBtn}`}>
                        <i className="material-icons">help_outline</i>
                    </a>
                    {/**Modal Help User */}
                    <div id="help" className="modal">
                        <div className="modal-content">
                            <h4 className={style.titleModal}>¿Necesitas ayuda?</h4>
                            <p>Si tienes alguna duda sobre el funcionamiento de Feedme, te ofrecemos este video para que resuelvas tus dudas rápidamente.</p>
                            <div className="video-container">
                                <iframe allowFullScreen title="Video tutorial feedme" frameBorder="no" width="1920" height="1080" src="https://wideo.co/embed/29382631591889037923?width=1920&height=1080&repeat=false&autoplay=false&html5=true"></iframe>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button className="modal-close waves-effect waves-green btn-flat">¡Estupendo!</button>
                        </div>
                    </div>
                    {/**Saved searches modal */}
                    <div id="saved-searches" className="modal">
                        <div className="modal-content">
                            <h4 className={style.titleModal}>Búsquedas guardadas</h4>
                            <p>Haz click en una de tus búsquedas guardadas para encontrar recetas.</p>
                            <ul className="collection">
                                {this.renderSavedSearches()}
                            </ul>
                        </div>
                        <div className="modal-footer">
                            <button className="modal-close waves-effect waves-green btn-flat">Cerrar</button>
                        </div>
                    </div>
                    <ModalRegister/>
                </main>
            )
        }
    }
}
