import React, { Component } from 'react';
import M from 'materialize-css';
import 'materialize-css';
import style from './founded-receipts.module.css';
import ModalRegister from '../commons/modal-register/ModalRegister';
import ReceiptsGrid from '../commons/receipts-grid/ReceiptsGrid';

//Services
import { makeReceiptsRequest } from '../../services/FormSearchService';
import UserService from '../../services/user.service';
import authHeader from '../../services/auth-header';
import userService from '../../services/user.service';
import { MAX_SAVED_SEARCHES } from '../../services/globals';

/**
 * Sends the search to API and recive recipes data
 *
 * @version 1.0.0
 * @author [Carlos Esteban](https://gitlab.com/iaw47999217)
 */
export default class FoundedReceipts extends Component {

    constructor(props) {
        super(props);
        this.state = {
            receipts: [],
            order: null,
            status: null,
            search: this.props.search,
            newSearchTitle: '',
            savedSearches: [],
            isSearchSaved: false,
            isLoggedIn: authHeader(),
        };
    }

    componentDidMount() {
        //Resolves a bug caused by rendering this component from a modal
        document.querySelector('body').style.overflow = "auto";

        var search = this.props.search;
        this.modalsIni();
        this.selectIni();
        this.scrollTop();

        //Si existe la búsqueda hacemos la petición al servicio
        if (search && search !== null && search !== undefined) {
            this.getReceiptsData(search);
            this.getSavedSearches();
        }
    }

    /**
     * This function initializes all the modals of this component
     * @param
     * @public
     */
    modalsIni = () => {
        var options = {};
        var elems = document.querySelectorAll('.modal');
        M.Modal.init(elems, options);
    }

    /**
     * Get the saved searches if the user is loggedIn
     * @param
     * @public
     */
    getSavedSearches = async() => {
        if (this.state.isLoggedIn !== null) {
            let searches = await UserService.getSavedSearches();
            this.setState({
                savedSearches: searches
            })
        }
    }

    /**
     * Save a new saved search
     * @param
     * @public
     */
    saveSearch = async() => {
        let newTitle = this.state.newSearchTitle;
        let newSearch = JSON.parse(this.state.search);

        if (this.state.isSearchSaved === false && newTitle.length !== 0) {
            await UserService.saveSearch(newTitle, newSearch)
                .then(response => {
                    M.toast({
                        html: `La búsqueda se ha guardado correctamente.`,
                        classes: 'rounded succedToast'
                    });
                    let modal = document.querySelector('#saved-searches');
                    var instance = M.Modal.getInstance(modal);
                    instance.close();

                    this.setState({
                        isSearchSaved: true
                    });
                })
                .catch(error => {
                    M.toast({
                        html: `Algo ha ido mal: ${error}`,
                        classes: 'rounded errorToast'
                    });
                })
        } else {
            M.toast({
                html: `Debes añadir un título para guardar la búsqueda.`,
                classes: 'rounded errorToast'
            })
            let inputTitle = document.querySelector('#newSearchTitle');
            inputTitle.classList.add('invalid');
        }
    }

    /**
     * Overwrite a saved search
     * @param
     * @public
     */
    overwriteSearch = async(searchId) => {
        let newTitle = this.state.newSearchTitle;
        let newSearch = JSON.parse(this.state.search);

        if (newTitle.length !== 0) {
            let newSearchObj = {
                    title: newTitle,
                    isvegan: newSearch.isvegan,
                    isvegetarian: newSearch.isvegetarian,
                    allergens: newSearch.allergens,
                    includedIngredients: newSearch.includedIngredients,
                    excludedIngredients: newSearch.excludedIngredients,
                    maxtime: newSearch.maxtime,
                    includedTags: []
                }
                //Mirar que parametro enviar
            await userService.updateSearch(searchId, newSearchObj)
                .then(response => {
                    M.toast({
                        html: `La búsqueda se ha sobreescrito correctamente.`,
                        classes: 'rounded succedToast'
                    })
                    let modal = document.querySelector('#saved-searches');
                    var instance = M.Modal.getInstance(modal);
                    instance.close();

                    this.setState({
                        isSearchSaved: true
                    })
                })
                .catch(error => {
                    M.toast({
                        html: `Algo ha ido mal: ${error}`,
                        classes: 'rounded errorToast'
                    })
                })
        } else {
            M.toast({
                html: `Debes añadir un título para sobreescribir la búsqueda.`,
                classes: 'rounded errorToast'
            })
            let inputTitle = document.querySelector('#newSearchTitle');
            inputTitle.classList.add('invalid');
        }
    }

    /**
     * Update primary search
     * @param
     * @public
     */
    updatePrimarySearch = async() => {
        const search = JSON.parse(this.state.search);

        const newPrimarySearch = {
            allergens: search.allergens,
            isvegan: search.isvegan,
            isvegetarian: search.isvegetarian,
            excludedIngredients: search.withoutTheseIngredients
        }

        await UserService.updatePrimarySearch(newPrimarySearch)
            .then(response => {
                M.toast({
                    html: `La búsqueda primária se ha actualizado`,
                    classes: 'rounded succedToast'
                })
                let modal = document.querySelector('#saved-searches');
                var instance = M.Modal.getInstance(modal);
                instance.close();
            })
            .catch(error => {
                M.toast({
                    html: `Ha habido un problema: ${error}`,
                    classes: 'rounded errorToast'
                })
            })
    }

    /**
     * Renders the save search button depending on whether the user is logged in or if they have already saved the search in the previous component
     * @param
     * @public
     */
    renderSaveSearchButton = () => {
        if (this.state.isLoggedIn !== null) {
            if (this.state.isSearchSaved) {
                return ( 
                    <a href = "#!" className = { `waves-effect waves-light disabled btn ${style.saveSearchBtn}` } > Guardar búsqueda </a>
                );
            } else {
                return ( 
                    <a href = "#saved-searches" className = { `waves-effect waves-light btn modal-trigger ${style.saveSearchBtn}` } > Guardar búsqueda </a>
                );
            }
        } else {
            return ( 
                <a href = "#register" className = { `waves-effect waves-light btn modal-trigger ${style.saveSearchBtn}` } > Guardar búsqueda </a>
            );
        }
    }

    /**
     * Renders saved searches list on saved-searches modal
     * @param
     * @public
     */
    renderSavedSearches = () => {
        if (this.state.savedSearches.length > 0) {
            let searches = this.state.savedSearches.map((item, i) => {
                return ( 
                    <li key={i} className="collection-item">
                        <div>{item.title || "Sin título"}
                            <a href="#!" onClick = {() => { this.overwriteSearch(item.id) }} className = "secondary-content">
                                <i className={ `material-icons ${style.brandColor}`}>autorenew</i>
                            </a>
                        </div>
                    </li>
                );
            });
            return searches;
        } else {
            return ( 
                <p className="center">No hay búsquedas guardadas.</p>
            );
        }
    }

    /**
     * Get the receipts data from the service function
     * @param search uses the search user did to get receipts
     * @public
     */
    getReceiptsData = async(search) => {
        if (this.state.isLoggedIn !== null) {
            await makeReceiptsRequest(search)
                .then(response => {
                    this.setState({
                        receipts: response.data.recipes,
                        order: "fav",
                        status: "success"
                    })
                })
                .catch(error => {
                    this.setState({
                        receipts: [],
                        status: "failed"
                    })
                    M.toast({
                        html: `Algo ha ido mal: ${error}`,
                        classes: 'rounded errorToast'
                    });
                })
        } else {
            await makeReceiptsRequest(search)
                .then(response => {
                    this.setState({
                        receipts: response.data.recipes,
                        order: "time",
                        status: "success"
                    })
                })
                .catch(error => {
                    this.setState({
                        receipts: [],
                        status: "failed"
                    })
                    M.toast({
                        html: `Algo ha ido mal: ${error}`,
                        classes: 'rounded errorToast'
                    });
                })
        }
        this.orderReceipts(this.state.order);
    }

    /**
     * Function that sorts the receipts results
     * @param order string that contains the order type
     * @public
     */
    orderReceipts = (order) => {
        if (order === "time") {
            this.setState({
                receipts: this.state.receipts.sort(this.sortByTime)
            })
        }
        if (order === "difficulty") {
            this.setState({
                receipts: this.state.receipts.sort(this.sortByDifficulty)
            })
        }
        if (order === "fav") {
            this.setState({
                receipts: this.state.receipts.sort(this.sortByFav)
            })
        }
    }

    /**
     * Sorts an array by favourites
     * @param a Element
     * @param b Element
     * @public
     */
    sortByFav = (a, b) => {
        return (a.favourite === b.favourite) ? 0 : a.favourite ? -1 : 1;
    }

    /**
     * Sorts an array by time
     * @param a Element
     * @param b Element
     * @public
     */
    sortByTime = (a, b) => {
        if (a.time < b.time) {
            return -1;
        }
        if (a.time > b.time) {
            return 1;
        }
        return 0;
    }

    /**
     * Sorts an array by difficulty
     * @param a
     * @param b
     * @public
     */
    sortByDifficulty = (a, b) => {
        if (a.difficulty < b.difficulty) {
            return -1;
        }
        if (a.difficulty > b.difficulty) {
            return 1;
        }
        return 0;
    }


    /**
     * Detects a change in the order pattern and updates it
     * @param e Event
     * @public
     */
    handleChangeOrder = (e) => {
        this.setState({
            order: e.target.value
        })
        this.orderReceipts(e.target.value);
    }

    /**
     * Detects a change in the title of the new search
     * @param
     * @public
     */
    handleChangeNewSearchTitle = (e) => {
        this.setState({
            newSearchTitle: e.target.value
        }, () => {
            if (this.state.newSearchTitle.length !== 0) {
                let titleInput = document.querySelector('#newSearchTitle');
                titleInput.classList.remove('invalid');
            }
        })
    }

    /**
     * Scrolls window to the top
     * @param
     * @public
     */
    scrollTop = () => {
        window.scrollTo({ top: 0 });
    }

    /**
     * This function initializes the select form in order to filter founded receipts
     * @param
     * @public
     */
    selectIni = () => {
        var options = {};
        var elems = document.querySelectorAll('select');
        M.FormSelect.init(elems, options);
    }

    render() {
        if (this.state.receipts.length >= 1 && this.state.status === "success"){
            
            return (
                <main className={style.main}>
                    <div className="container">
                        <div className={`row ${style.mb0}`}>
                            <div className="col s12">
                                <h1 className={`center ${style.title}`}>Resultados de búsqueda</h1>
                            </div>
                        </div>
                        <div className={`divider ${style.dividerLane}`}></div>
                        <div className={`row ${style.mb0}`}>
                            <div className={`col s12 ${style.optionsRow}`}>
                                <div className={`row ${style.mb0}`}>
                                    <div className="col s12 m6 center">
                                        {this.renderSaveSearchButton()}
                                        
                                    </div>
                                    <div className="col s12 m6 center">
                                        <div className={`row ${style.mb0}`}>
                                            <div className="col s5 m4 right-align">
                                                <p>Ordenar:</p>
                                            </div>
                                            <div className="col s5 m5 l4 left-align">
                                                {
                                                    this.state.isLoggedIn !== null ? (
                                                        <select
                                                            onChange={this.handleChangeOrder}>
                                                            <option value="fav" defaultValue >Por favoritas</option>
                                                            <option value="time">Por tiempo</option>
                                                            <option value="difficulty">Por dificultad</option>
                                                        </select>
                                                    ) : (
                                                        <select
                                                            onChange={this.handleChangeOrder}>
                                                            <option value="time" defaultValue >Por tiempo</option>
                                                            <option value="difficulty">Por dificultad</option>
                                                        </select>
                                                    )
                                                }
                                            </div>
                                        </div>                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ReceiptsGrid
                            receipts={this.state.receipts}
                        />
                    </div>
                    {/**Saved searches modal */}
                    <div id="saved-searches" className="modal modal-fixed-footer">
                        <div className="modal-content">
                            <h4 className={style.titleModal}>Guardar búsquedas</h4>
                            {this.state.savedSearches.length < MAX_SAVED_SEARCHES ? (
                                <React.Fragment>
                                <p>Guarda la búsqueda reciente para poder utilizarla cuando quieras.</p>
                                <p>También puedes sobreescribir una búsqueda anterior añadiendo un título y seleccionando la búsqueda que quieras sobreescribir.</p>
                                <form>
                                    <div className="input-field col s6">
                                        <input 
                                            id="newSearchTitle" 
                                            name="newSearchTitle" 
                                            type="text"
                                            onChange={this.handleChangeNewSearchTitle}
                                        />
                                        <label htmlFor="newSearchTitle">Título de la nueva búsqueda</label>
                                    </div>
                                </form>
                                <p>Búsquedas guardadas:</p>
                                <ul className="collection">
                                    {this.renderSavedSearches()}
                                </ul>
                                <div className="col s12">
                                    <button onClick={() => this.saveSearch()} className={`waves-effect waves-light btn ${style.searchReceiptsBtn}`}>Guardar nueva búsqueda</button>
                                </div>
                                <br/>
                                <p>Si quieres actualizar sólamente la búsqueda primaria haz click aquí:</p>
                                <div className="col s12">
                                    <button onClick={()=> this.updatePrimarySearch()} className={`waves-effect waves-light btn ${style.updatePrimarySearchBtn}`}>Actualizar búsqueda primária</button>
                                </div>
                                </React.Fragment>
                            ) : (
                                <React.Fragment>
                                <p><i className={`material-icons ${style.brandColor}`}>warning</i>Has llegado al límite de búsquedas guardadas.</p>
                                <p>Si quieres guardar la búsqueda actual, escribe un título y después selecciona una búsqueda anterior para sobreescribirla.</p>
                                <form>
                                    <div className="input-field col s6">
                                        <input 
                                            id="newSearchTitle" 
                                            name="newSearchTitle" 
                                            type="text" 
                                            onChange={this.handleChangeNewSearchTitle}
                                        />
                                        <label htmlFor="newSearchTitle">Título de la nueva búsqueda</label>
                                    </div>
                                </form>
                                <p>Búsquedas guardadas:</p>
                                <ul className="collection">
                                    {this.renderSavedSearches()}
                                </ul>
                                <br/>
                                <p>Si quieres actualizar sólamente la búsqueda primaria haz click aquí:</p>
                                <div className="col s12">
                                    <button onClick={()=> this.updatePrimarySearch()} className={`waves-effect waves-light btn ${style.updatePrimarySearchBtn}`}>Actualizar búsqueda primária</button>
                                </div>
                                </React.Fragment>
                            )}
                        </div>
                        <div className="modal-footer">
                            <button className="modal-close waves-effect waves-green btn-flat">Cerrar</button>
                        </div>
                    </div>
                    <ModalRegister/>
                </main>
            )
        }
        else if (this.state.status === "failed") {
            return(
                <div className="col s12 center">
                    <p>Ha habido un error. Intentalo de nuevo más tarde..</p>
                </div>
            );
        }
        else if (this.state.receipts.length === 0 && this.state.status === "success"){
            return(
                <main className={style.main}>
                    <div className="container">
                        <div className={`row ${style.mb0}`}>
                            <div className="col s12">
                                <h1 className={`center ${style.title}`}>Resultados de búsqueda</h1>
                            </div>
                        </div>
                        <div className={`divider ${style.dividerLane}`}></div>
                        <div className={`row ${style.mb0}`}>
                            <div className={`col s12 ${style.optionsRow}`}>
                                <div className={`row ${style.mb0}`}>
                                    <div className="col s12 m6 center">
                                        <a href="#register" className={`waves-effect waves-light btn disabled modal-trigger ${style.saveSearchBtn}`}>Guardar búsqueda</a>
                                    </div>
                                    <div className="col s12 m6 center">
                                        <div className={`row ${style.mb0}`}>
                                            <div className="col s5 m4 right-align">
                                                <p>Ordenar:</p>
                                            </div>
                                            <div className="col s5 m5 l4 left-align">
                                                {
                                                    this.state.isLoggedIn !== null ? (
                                                        <select
                                                            onChange={this.handleChangeOrder}>
                                                            <option value="fav" defaultValue >Por favoritas</option>
                                                            <option value="time">Por tiempo</option>
                                                            <option value="difficulty">Por dificultad</option>
                                                        </select>
                                                    ) : (
                                                        <select
                                                            onChange={this.handleChangeOrder}>
                                                            <option value="time" defaultValue >Por tiempo</option>
                                                            <option value="difficulty">Por dificultad</option>
                                                        </select>
                                                    )
                                                }
                                            </div>
                                        </div>                            
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div className={`row ${style.mb0} ${style.fullHeight} center`}>
                            No hay resultados.
                        </div>
                    </div>
                    <ModalRegister/>
                </main>
            );
        } else {
            return(
                <main className={style.main}>
                    <div className="container">
                        <div className={`row ${style.mb0}`}>
                            <div className="col s12">
                                <h1 className={`center ${style.title}`}>Resultados de búsqueda</h1>
                            </div>
                        </div>
                        <div className={`divider ${style.dividerLane}`}></div>
                        <div className={`row ${style.mb0}`}>
                            <div className={`col s12 ${style.optionsRow}`}>
                                <div className={`row ${style.mb0}`}>
                                    <div className="col s12 m6 center">
                                        <a href="#register" className={`waves-effect waves-light btn modal-trigger ${style.saveSearchBtn}`}>Guardar búsqueda</a>
                                    </div>
                                    <div className="col s12 m6 center">
                                        <div className={`row ${style.mb0}`}>
                                            <div className="col s5 m4 right-align">
                                                <p>Ordenar:</p>
                                            </div>
                                            <div className="col s5 m5 l4 left-align">
                                                {
                                                    this.state.isLoggedIn !== null ? (
                                                        <select
                                                            onChange={this.handleChangeOrder}>
                                                            <option value="fav" defaultValue >Por favoritas</option>
                                                            <option value="time">Por tiempo</option>
                                                            <option value="difficulty">Por dificultad</option>
                                                        </select>
                                                    ) : (
                                                        <select
                                                            onChange={this.handleChangeOrder}>
                                                            <option value="time" defaultValue >Por tiempo</option>
                                                            <option value="difficulty">Por dificultad</option>
                                                        </select>
                                                    )
                                                }
                                            </div>
                                        </div>                            
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div className={`row ${style.mb0} ${style.fullHeight} center`}>
                            <div className="preloader-wrapper big active ">
                                <div className="spinner-layer spinner-blue ">
                                    <div className="circle-clipper left">
                                    <div className="circle"></div>
                                    </div><div className="gap-patch">
                                    <div className="circle"></div>
                                    </div><div className="circle-clipper right">
                                    <div className="circle"></div>
                                    </div>
                                </div>
                                <div className="spinner-layer spinner-red">
                                    <div className="circle-clipper left">
                                    <div className="circle"></div>
                                    </div><div className="gap-patch">
                                    <div className="circle"></div>
                                    </div><div className="circle-clipper right">
                                    <div className="circle"></div>
                                    </div>
                                </div>
                                <div className="spinner-layer spinner-yellow">
                                    <div className="circle-clipper left">
                                    <div className="circle"></div>
                                    </div><div className="gap-patch">
                                    <div className="circle"></div>
                                    </div><div className="circle-clipper right">
                                    <div className="circle"></div>
                                    </div>
                                </div>
                                <div className="spinner-layer spinner-green">
                                    <div className="circle-clipper left">
                                    <div className="circle"></div>
                                    </div><div className="gap-patch">
                                    <div className="circle"></div>
                                    </div><div className="circle-clipper right">
                                    <div className="circle"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ModalRegister/>
                </main>
            );
        }
    }
}