import React, { Component } from 'react';

//Styles imports
import 'materialize-css/dist/css/materialize.min.css';
import M from 'materialize-css';
import style from './primary-search.module.css';

//Services imports
import { getIngredients, getAllergens } from '../../services/FormSearchService';
import userService from '../../services/user.service';
import { Redirect } from 'react-router-dom';
import { apiResponseToChipsFormat, chipsFormatToApiRequest } from '../../services/globals';

/**
 * Component to edit primary search.
 *
 * @version 1.0.0
 * @author [Carlos Esteban](https://gitlab.com/iaw47999217)
 */
export default class PrimarySearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            doNotHaveTheseIngredients: [],
            ingredientsApiFormat: [],
            ingredientsAutocomplete: {},
            isvegetarian: false,
            isvegan: false,
            radioBtnChecked: false,
            allergens: [],
            allergensApiFormat: [],
            allergensAutocomplete: {},
            primarySearch: {},
            redirect: false
        }
    }

    componentDidMount() {
        this.scrollTop();
        this.getDataAutocompleteLists();
    }

    /**
     * Scrolls window to the top
     * @public
     */
    scrollTop = () => {
        window.scrollTo({ top: 0 });
    }

    /**
     * Function that provides lists of data from FormSearchService to use on autocomplete lists
     * @param
     * @public
     */
    getDataAutocompleteLists = async() => {
        await getIngredients()
            .then(ingredients => {
                let ingredientChipsFormatObj = apiResponseToChipsFormat(ingredients);
                this.setState({
                    ingredientsApiFormat: ingredients,
                    ingredientsAutocomplete: ingredientChipsFormatObj
                })
            })
            .catch(error => {
                M.toast({
                    html:`${error}`,
                    classes:'rounded errorToast'
                })
            })

        await getAllergens()
            .then(allergens => {
                let allergensChipsFormatObj = apiResponseToChipsFormat(allergens);
                this.setState({
                    allergensApiFormat: allergens,
                    allergensAutocomplete: allergensChipsFormatObj
                });
            })
            .catch(error => {
                M.toast({
                    html:`${error}`,
                    classes:'rounded errorToast'
                })
            })

        await userService.getPrimarySearch()
            .then(primarySearch => {
                this.setState({
                    primarySearch: primarySearch,
                    doNotHaveTheseIngredients: primarySearch.doNotHaveTheseIngredients,
                    isvegetarian: primarySearch.isvegetarian,
                    isvegan: primarySearch.isvegan,
                    allergens: primarySearch.allergens
                }, () => {
                    if (this.state.isvegetarian) {
                        document.primarySearch.dietType[0].checked = true
                        this.setState({
                            radioBtnChecked: true
                        })
                    } else if (this.state.isvegan) {
                        document.primarySearch.dietType[1].checked = true
                        this.setState({
                            radioBtnChecked: true
                        })
                    } else if (!this.state.isvegetarian && !this.state.isvegan) {
                        document.primarySearch.dietType[2].checked = true
                        this.setState({
                            radioBtnChecked: true
                        })
                    }
                });
                //Once the data is recived, initialize chips 
                this.chipsDoNotHaveIngredientsIni();
                this.chipsAllergensIni();
            })
            .catch(error => {
                M.toast({
                    html:`${error}`,
                    classes:'rounded errorToast'
                })
            })
    }

    /**
     * Submits the form and saves the primary search calling the service
     * @param event
     * @public
     */
    handleSubmit = (event) => {
        event.preventDefault();

        const {
            doNotHaveTheseIngredients,
            isvegetarian,
            isvegan,
            radioBtnChecked,
            allergens
        } = this.state;

        if (!radioBtnChecked) {
            //Si el radiobutton no esta marcado mostramos un error al usuario y cambiamos el color del label
            let label = document.querySelector('#dietLabel');
            label.classList.add('invalid');
            M.toast({
                html: `Tienes que añadir el tipo de dieta para guardar la búsqueda primaria`,
                classes: 'rounded errorToast'
            })
        } else {
            let primarySearch = {
                withoutTheseIngredients: doNotHaveTheseIngredients,
                isvegetarian: isvegetarian,
                isvegan: isvegan,
                allergens: allergens,
            }

            this.setState({
                primarySearch: primarySearch
            }, async() => {
                //Obtener ID alergenos y ingredientes
                const searchWithIDs = chipsFormatToApiRequest(this.state.primarySearch, this.state.ingredientsApiFormat,
                    this.state.allergensApiFormat, true);

                await userService.updatePrimarySearch(searchWithIDs)
                    .then(response => {
                        M.toast({
                            html: `Búsqueda primaria guardada correctamente`,
                            classes: 'rounded succedToast'
                        });
                        this.setState({
                            redirect: true
                        })
                    })
                    .catch(error => {
                        M.toast({
                            html: `La búsqueda primaria no ha podido guardarse, intentalo de nuevo. ${error}`,
                            classes: 'rounded errorToast'
                        });
                    })
            })
        }
    }

    /**
     * Evaluates if radio buttons are selected
     * @param
     * @public
     */
    evalDiet = () => {
        let dietRadioForm = document.primarySearch.dietType;
        //Si la dieta  esta seleccionada
        if (dietRadioForm.value === "on") {
            //cambiamos el estado
            this.setState({
                    radioBtnChecked: true
                })
                //Quitamos el aviso al usuario
            let label = document.querySelector('#dietLabel');
            label.classList.remove('invalid');
        } else {
            //Mantenemos el state en falso
            this.setState({
                radioBtnChecked: false
            })
        }
    }

    /**
     * Changes the vegetarian diet state
     * @param
     * @public
     */
    handleVegetarianDietChange = () => {
        this.setState({
            isvegetarian: true,
            isvegan: false
        })
        this.evalDiet();
    }

    /**
     * Changes the vegan diet state
     * @param
     * @public
     */
    handleVeganDietChange = () => {
        this.setState({
            isvegetarian: false,
            isvegan: true
        })
        this.evalDiet();
    }

    /**
     * Changes the vegan and the vegetarian diet state
     * @param
     * @public
     */
    handleAnyTypeOfDietChange = () => {
        this.setState({
            isvegetarian: false,
            isvegan: false
        })
        this.evalDiet();
    }

    /**
     * Initializes chips on the haveIngredients input
     * @param
     * @public
     */
    chipsDoNotHaveIngredientsIni = () => {
        var elems = document.querySelector('.doNotHaveTheseIngredients');
        var options = {
            //Añade ingredientes por defecto
            data: this.state.doNotHaveTheseIngredients,
            //Ingredientes de la base de datos
            autocompleteOptions: {
                data: this.state.ingredientsAutocomplete
            },
            limit: 20,
            secondaryPlaceholder: "+ ingredientes",
            onChipAdd: function(e, chip) { //Esta función actua como eventListener al añadir un ingrediente
                var item = chip.childNodes[0].textContent;
                //Comprueba si el ingrediente existe en la BD
                let existOnDB = this.state.ingredientsAutocomplete[item] !== null;
                let existOnOtherInput = false;
                //Se comprueba si el ingrediente añadido existe en la lista de alimentos existente en la BD
                // y tampoco existe en en input de ingredientes que contenga la receta                  
                if (existOnDB || existOnOtherInput) {
                    //En el caso de que no exista ese ingrediente, se elimina el ingrediente del input
                    let doNotHaveTheseIngredients = document.querySelector('.doNotHaveTheseIngredients');
                    let instance = M.Chips.getInstance(doNotHaveTheseIngredients);
                    //Eliminamos el ultimo ingrediente, que es el que se acaba de añadir
                    instance.deleteChip(instance.chipsData.length - 1);
                } else {
                    //Si existe el ingrediente se añade al state
                    this.setState({
                        doNotHaveTheseIngredients: e[0].M_Chips.chipsData
                    });
                }
            }.bind(this),
            onChipDelete: function(e, chip) {}
        };
        M.Chips.init(elems, options);
    }

    /**
     * Initializes chips on the allergies input
     * @param
     * @public
     */
    chipsAllergensIni = () => {
        var elems = document.querySelector('.allergies');
        var options = {
            //Añade ingredientes por defecto
            data: this.state.allergens,
            //Ingredientes de la base de datos
            autocompleteOptions: {
                data: this.state.allergensAutocomplete
            },
            limit: 20,
            secondaryPlaceholder: "+ alergias/intolerancias",
            onChipAdd: function(e, chip) { //Esta función actua como eventListener al añadir un alérgeno
                var item = chip.childNodes[0].textContent;
                //Se comprueba si el alergeno añadido existe en la lista de alergenos de la BD
                if (this.state.allergensAutocomplete[item] !== null) {
                    //En el caso de que no exista ese alergeno, se elimina el alergeno del input
                    let allergens = document.querySelector('.allergies');
                    let instance = M.Chips.getInstance(allergens);
                    //Eliminamos el ultimo ingrediente, que es el que se acaba de añadir
                    instance.deleteChip(instance.chipsData.length - 1);
                } else {
                    //Si existe el alergeno se añade al state
                    this.setState({
                        allergens: e[0].M_Chips.chipsData
                    });
                }
            }.bind(this)
        };
        M.Chips.init(elems, options);
    }

    render() {
        if (this.state.redirect) {
            return ( 
                < Redirect to = { '/perfil' } />
            );
        }

        return (
            <main className={style.main}>
                <div className="container">
                    <div className={`row ${style.mb0}`}>
                        <div className={`col s12 center ${style.titleDiv}`}>
                            <h1 className={style.title}>Configura tu búsqueda primaria</h1>
                            <br/>
                            <p>La búsqueda primaria se aplica a todas tus búsquedas en Feedme, aquí puedes configurar tus alergias e intolerancias, tu tipo de dieta e incluso añadir ingredientes que no te gusten...</p>
                        </div>
                        <div className={`col s12 push-l2 l8 ${style.formDiv}`}>
                            <form 
                                onSubmit={this.handleSubmit}
                                name="primarySearch"
                            >
                                <div className="row">
                                    <div className="col s12">
                                        <div className="row">
                                            <div className="input-field col s12">
                                                <i className="material-icons prefix">remove_circle</i>
                                                <div className={`chips chips-autocomplete doNotHaveTheseIngredients ${style.mt40}`}></div>
                                                <label htmlFor="">No quiero que las recetas contengan estos ingredientes</label>
                                            </div>
                                        </div>
                                    </div>
                                    {/* Input dietas */}
                                    <div className="col s12">
                                        <div className="row">
                                            <div className="input-field col s12">
                                                <i className="material-icons prefix">restaurant</i>
                                                <div className={`col s6 push-s4 ${style.mt40}`}>
                                                    <p>
                                                        <label>
                                                            <input 
                                                                className="with-gap" 
                                                                name="dietType" 
                                                                type="radio"
                                                                onChange = {this.handleVegetarianDietChange}
                                                            />
                                                            <span>Vegetariana</span>
                                                        </label>
                                                    </p>
                                                    <p>
                                                        <label>
                                                            <input 
                                                                className="with-gap" 
                                                                name="dietType" 
                                                                type="radio"
                                                                onChange = {this.handleVeganDietChange} 
                                                            />
                                                            <span>Vegana</span>
                                                        </label>
                                                    </p>
                                                    <p>
                                                        <label>
                                                            <input 
                                                                className="with-gap" 
                                                                name="dietType" 
                                                                type="radio" 
                                                                onChange = {this.handleAnyTypeOfDietChange}
                                                            />
                                                            <span>Ninguna en especial</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <label id="dietLabel" htmlFor="">Mi dieta es...</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col s12">
                                        <div className="row">
                                            <div className="input-field col s12">
                                                <i className="material-icons prefix">warning</i>
                                                <div className={`chips chips-autocomplete allergies ${style.mt40}`}></div>
                                                <label htmlFor="">Mis alergias / intolerencias son...</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col s12">
                                    <div className="row">
                                        <div className="input-field col s12 center">
                                            <button 
                                                type="submit" 
                                                className={`waves-effect waves-light btn ${style.saveSearchBtn}`}
                                            >
                                                Guardar búsqueda primaria
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}