import React, { Component } from 'react';
import M from 'materialize-css';
import 'materialize-css';
import style from './user-detail.module.css';
import ReceiptsGrid from '../commons/receipts-grid/ReceiptsGrid';

//Services
import userService from '../../services/user.service';
import authService from '../../services/auth.service';
import { Redirect, NavLink } from 'react-router-dom';
import { getIdFromApiFormatObj } from '../../services/globals';

/**
 * User personal profile.
 *
 * @version 1.0.0
 * @author [Carlos Esteban](https://gitlab.com/iaw47999217)
 */
export default class UserDetail extends Component {

    constructor(props){
        super(props);
        this.state = {
            user: {},
            receipts: [],
            status: null,
            redirect: false,
            actualPassword: '',
            newPassword: '',
            confirmNewPassword: '',
            savedSearches: [],
            search: {},
            doSavedSearch: false,
            deletedAccount: false,
            userName: '',
            userEmail: ''
        };
    }

    componentDidMount(){
        this.getFavReceipts();
        this.getUserData();
        this.getSavedSearches();
        this.tabsIni();
        this.modalsIni();
        this.initTooltips();
    }

    /**
     * Get user saved searches
     * @param
     * @public
     */
    getSavedSearches = async() => {
        let searches = await userService.getSavedSearches();
        this.setState({
            savedSearches: searches
        })
    }

    /**
     * Renders all the user saved searches
     * @param
     * @public
     */
    renderSavedSearches = () => {
        if (this.state.savedSearches.length > 0) {
            let searches = this.state.savedSearches.map((item,i) => {
                return(
                    <div key={i} className={`col s12  ${style.mb20}`}>
                        <div className={`col s6 ${style.sectionOption}`}>
                            <a href="#!" onClick={() => {this.doSavedSearch(item.id)}}>
                                <span>{item.title || "Sin título"}</span>
                            </a>
                        </div>
                        <div className="col s6">
                            <NavLink to={'/editar-busqueda/'+ item.id} className={`btn waves-effect waves-light ${style.settingBtn}`}>
                                <i className="tiny material-icons tooltipped" data-position="bottom" data-tooltip="Editar búsqueda">settings</i>
                            </NavLink>
                            <a href="#!" onClick={() => {this.deleteSearch(item.id)}} className={`btn waves-effect waves-light ${style.settingBtn}`}>
                                <i className="tiny material-icons tooltipped" data-position="bottom" data-tooltip="Elimina búsqueda">delete</i>
                            </a>
                        </div>
                    </div>
                );
            })
            return searches;
        } else {
            return (
                <div className={`col s12 center  ${style.mb20}`}>
                    <p>No hay búsquedas guardadas.</p>
                </div>
            );
        }
    }

    /**
     * Function that makes the redirection and passes the search object
     * @param searchToDo Search id
     * @public
     */
    doSavedSearch = async(searchId) => {
        await userService.getStoredSearch(searchId)
            .then( storedSearch => {
                let searchObj = {
                    excludedIngredients: getIdFromApiFormatObj(storedSearch.excludedIngredients),
                    includedIngredients: getIdFromApiFormatObj(storedSearch.includedIngredients),
                    allergens: getIdFromApiFormatObj(storedSearch.allergensOrig),
                    isvegan: storedSearch.isvegan,
                    isvegetarian: storedSearch.isvegetarian,
                    maxtime: storedSearch.maxtime,
                    tags: []
                }
                this.setState({
                    search: searchObj,
                    redirect: true,
                    doSavedSearch: true
                })
            })
            .catch(error => {
                M.toast({
                    html:`${error}`,
                    classes:'rounded errorToast'
                })
            })
    }

    /**
     * Delete the clicked saved search
     * @param idSearch Id of the saved search
     * @public
     */
    deleteSearch = async(idSearch) => {
        await userService.deleteSearch(idSearch);

        let savedSearches = [...this.state.savedSearches];
        savedSearches.forEach((element,i) => {
            if (element.id === idSearch) {
                savedSearches.splice(i,1);
            }
        });
        this.setState({
            savedSearches: savedSearches
        })
    }

    /**
     * Get user data
     * @param
     * @public
     */
    getUserData = async() => {
        try {
            const response = await userService.getUserProfileData();
                this.setState({
                    user: response.data.user,
                    userName: response.data.user.name,
                    userEmail: response.data.user.email
                })
        } catch (error) {
            M.toast({
                html:`No es posible recibir datos de tu usuario: ${error}`,
                classes:'rounded errorToast',
                displayLength: 20000
            })
        }
    }

    /**
     * Get user favourite receipts
     * @param
     * @public
     */
    getFavReceipts = async() => {

        await userService.getFavReceipts()
            .then(favRecipes => {
                    this.setState({
                        receipts: favRecipes,
                        status: "success"
                    })
            })
            .catch(error => {
                M.toast({
                    html: `Algo ha ido mal. ${error}`,
                    classes: 'rounded errorToast'
                });
                this.setState({
                    receipts: [],
                    status: "failed"
                })
            })
    }

    /**
     * Makes the call to changePassword service on UserService
     * @param
     * @public
     */
    changePassword = async() => {
        
        //If the new password accomplishes the requirements, the service is called
        if (this.state.actualPassword !== this.state.newPassword 
            && this.state.newPassword.length >= 8 
            && this.state.actualPassword.length >= 8
            && this.state.newPassword === this.state.confirmNewPassword) {
            
            await userService.changePassword(this.state.actualPassword, this.state.newPassword, this.state.confirmNewPassword)
                .then(response => {
                    M.toast({
                        html: `Tu nueva contraseña se ha cambiado correctamente.`,
                        classes: 'rounded succedToast'
                    });
                    let modalPassword = document.querySelector('#change-password');
                    var instance = M.Modal.getInstance(modalPassword);
                    instance.close();
                })
                .catch(error => {
                    M.toast({
                        html: `Tu nueva contraseña no ha podido cambiarse. ${error}`,
                        classes: 'rounded errorToast'
                    });
                })
            
        } else {
            if (this.state.newPassword.length < 8) {
                M.toast({
                    html: `Tu nueva contraseña debe tener al menos 8 carácteres.`,
                    classes: 'rounded errorToast'
                });
            } else if (this.state.actualPassword === this.state.newPassword) {
                M.toast({
                    html: `Tu nueva contraseña no puede ser igual que la anterior.`,
                    classes: 'rounded errorToast'
                });
            } else if (this.state.actualPassword < 8) {
                M.toast({
                    html: `Tu contraseña anterior es incorrecta.`,
                    classes: 'rounded errorToast'
                });
            } else if (this.state.newPassword !== this.state.confirmNewPassword) {
                M.toast({
                    html: `Confirma la nueva contraseña correctamente.`,
                    classes: 'rounded errorToast'
                });
            }
        }
    }

    /**
     * Controlls the change of the inputs in the change-password modal.
     * @param
     * @public
     */
    handleChangePasswords = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        }, () => {
            let newPasswordInput = document.querySelector('#new_password');
            let confirmNewPasswordInput = document.querySelector('#confirm_new_password');
            
            if (this.state.actualPassword !== this.state.newPassword 
                && this.state.newPassword.length >= 8 
                && this.state.actualPassword.length >= 8
                && this.state.newPassword === this.state.confirmNewPassword) {
                newPasswordInput.classList.remove('invalid');
                confirmNewPasswordInput.classList.remove('invalid');
                newPasswordInput.classList.add('valid');
                confirmNewPasswordInput.classList.add('valid');
            } else {
                newPasswordInput.classList.remove('valid');
                confirmNewPasswordInput.classList.remove('valid');
                newPasswordInput.classList.add('invalid');
                confirmNewPasswordInput.classList.add('invalid');
            }
        })
    }

    /**
     * Makes the call to deleteAccount service on UserService
     * @param
     * @public
     */
    deleteAccount = async() => {

        await userService.deleteAccount()
            .then(response => {
                M.toast({
                    html: `Cuenta eliminada con éxito.`,
                    classes: 'rounded succedToast'
                });
                this.setState({
                    redirect: true,
                    deletedAccount: true
                })
                authService.logout();
            })
            .catch(error => {
                M.toast({
                    html: `No se ha podido eliminar tu cuenta. Intentalo de nuevo. ${error}`,
                    classes: 'rounded errorToast'
                });
            })
    }

    /**
     * Initializes all the modals
     * @param
     * @public
     */
    modalsIni = () =>{
        var elems = document.querySelectorAll('.modal');
        M.Modal.init(elems, {});
    }

    /**
     * Initializes UserDetail swipeable tabs
     * @param
     * @public
     */
    tabsIni = () => {
        var options = {
            
        };
        var el = document.querySelector('.tabs');
        M.Tabs.init(el, options);
    }

    /**
     * This function initialize tooltips of the icons
     * @param
     * @public
     */
    initTooltips = () => {
        var options = {}
        var elems = document.querySelectorAll('.tooltipped');
        M.Tooltip.init(elems, options);
    }

    /**
     * Prints the grid depends on the status
     * @public
     */
    printGrid = () =>{
        if (this.state.receipts.length >=1 && this.state.status === "success") {
            return(
                <ReceiptsGrid
                    receipts={this.state.receipts}
                />
            );
        } else if (this.state.status === "failed") {
            return(
                <p>Ha habido un error</p> 
            ); 
        }
        else if (this.state.receipts.length === 0 && this.state.status === "success") {
            return(
                <p>No hay recetas favoritas que mostrar.</p> 
            ); 
        }
        else{
            return(
                <div className="preloader-wrapper big active ">
                    <div className="spinner-layer spinner-blue ">
                        <div className="circle-clipper left">
                        <div className="circle"></div>
                        </div><div className="gap-patch">
                        <div className="circle"></div>
                        </div><div className="circle-clipper right">
                        <div className="circle"></div>
                        </div>
                    </div>
                    <div className="spinner-layer spinner-red">
                        <div className="circle-clipper left">
                        <div className="circle"></div>
                        </div><div className="gap-patch">
                        <div className="circle"></div>
                        </div><div className="circle-clipper right">
                        <div className="circle"></div>
                        </div>
                    </div>
                    <div className="spinner-layer spinner-yellow">
                        <div className="circle-clipper left">
                        <div className="circle"></div>
                        </div><div className="gap-patch">
                        <div className="circle"></div>
                        </div><div className="circle-clipper right">
                        <div className="circle"></div>
                        </div>
                    </div>
                    <div className="spinner-layer spinner-green">
                        <div className="circle-clipper left">
                        <div className="circle"></div>
                        </div><div className="gap-patch">
                        <div className="circle"></div>
                        </div><div className="circle-clipper right">
                        <div className="circle"></div>
                        </div>
                    </div>
                </div>
            );
        }
    };

    render() {

        //Redirects to searchResults component after doing the redirect
        if (this.state.redirect && this.state.doSavedSearch) {
            return(
                <Redirect to={'/redirect/' + JSON.stringify(this.state.search)} />
            );
        }

        //Redirects to home if the account is deleted
        if (this.state.redirect && this.state.deletedAccount) {
            return(
                <Redirect to={'/'} />
            );
        }
        
        return (
            <main className={style.main}>
                <div className="container">
                    <div className={`row ${style.mb0}`}>
                        <div className={`col s12 ${style.tabsDiv}`}>
                            <ul className="tabs tabs-fixed-width">
                                <li className="tab col s6"><a href="#recetas-favoritas">recetas favoritas</a></li>
                                <li className="tab col s6"><a className="active" href="#perfil">Mi perfil</a></li>
                            </ul>
                        </div>
                        <div id="recetas-favoritas" className={`col s12 center ${style.favReceiptsDiv}`}>
                            <div className="row">
                                {this.printGrid()}
                                
                            </div>
                        </div>
                        <div id="perfil" className="col s12">
                            <div className={`col s12 ${style.titleProfileDiv}`}>
                                <h2 className={`${style.titleProfile}`}>información del usuario</h2>
                            </div>
                            <div className="col s12">
                                <div className="row">
                                    <div className={`col s12 push-l3 l6 ${style.spanInput}`}>
                                        <span>Nombre</span>
                                    </div>
                                    <div className="col s12 l6">
                                        <div className="input-field col s12 l6">
                                            <input readOnly value={this.state.userName} id="user_name" type="text" className="validate" disabled />
                                        </div>
                                    </div>
                                    <div className={`col s12 push-l3  l6 ${style.spanInput}`}>
                                        <span>Correo electronico</span>
                                    </div>
                                    <div className="col s12 l6">
                                        <div className="input-field col s12 l6">
                                            <input readOnly value={this.state.userEmail} id="user_email" type="text" className="validate" disabled />
                                        </div>
                                    </div>
                                    <div className={`col s12 push-l3  l6 ${style.spanInput}`}>
                                        <span>Contraseña</span>
                                    </div>
                                    <div className={`col s12 l6 ${style.changePassDiv}`}>
                                        <div className="col s6">
                                            <span className={style.receiptTime}>Cambiar contraseña</span>
                                        </div>
                                        <div className="col s6">
                                            <a href="#change-password" className={`btn waves-effect waves-light modal-trigger ${style.settingBtn}`}>
                                                <i className="tiny material-icons">settings</i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={`col s12 ${style.titleProfileDiv}`}>
                                <h2 className={`${style.titleProfile}`}>editar busqueda primaria</h2>
                            </div>
                            <div className="col s12">
                                <div className={`col s6 ${style.sectionOption}`}>
                                    <span>Busqueda primaria</span>
                                </div>
                                <div className="col s6">
                                    <NavLink to={'/busqueda-primaria'} className={`btn waves-effect waves-light ${style.settingBtn}`}>
                                        <i className="tiny material-icons">settings</i>
                                    </NavLink>
                                </div>
                            </div>
                            <div className={`col s12 ${style.titleProfileDiv}`}>
                                <h2 className={`${style.titleProfile}`}>búsquedas guardadas</h2>
                            </div>
                            {/*User saved searches */}
                            {this.renderSavedSearches()}
                            <div className={`col s12 ${style.titleProfileDiv}`}>
                                <h2 className={` ${style.titleProfile}`}>cambios en la cuenta</h2>
                            </div>
                            <div className={`col s12 ${style.mb20}`}>
                                <div className={`col s12 l6 ${style.sectionOption} ${style.mb20}`}>
                                    <span>Elimina tu cuenta y los datos de tu cuenta</span>
                                </div>
                                <div className={`col s12 l6 ${style.favBtnDiv} ${style.mb20} `}>
                                    <button data-target="delete-account" className={`waves-effect waves-light modal-trigger btn ${style.favBtn}`}><i className={`material-icons left ${style.favIcon}`}>delete</i>eliminar cuenta</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Delete account Modal */}
                <div id="delete-account" className="modal">
                    <div className="modal-content">
                    <h4>Eliminar cuenta</h4>
                    <p>¿Estás seguro que deseas eliminar tu cuenta de Feedme?</p>
                    <p>Se eliminarán todos tus datos y tus búsquedas, también las recetas que hayas marcado como favoritas.</p>
                    </div>
                    <div className="modal-footer">
                        <button onClick={this.deleteAccount} className="modal-close waves-effect waves-green btn-flat">Si</button>
                        <button className="modal-close waves-effect waves-green btn-flat">No</button>
                    </div>
                </div>
                {/* Change password Modal */}
                <div id="change-password" className="modal">
                    <div className="modal-content">
                        <h4 className="center">Cambiar contraseña</h4>
                        <p className="center">Introduce tu contraseña actual y después tu nueva contraseña.</p>
                        <form>
                            <div className="row">
                                <div className="input-field col push-l3 l6 s12">
                                    <input 
                                        id="actual_password" 
                                        name="actualPassword"
                                        type="password"
                                        onChange={this.handleChangePasswords} 
                                    />
                                    <label htmlFor="actual_password">Contraseña actual</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col push-l3 l6 s12">
                                    <input 
                                        id="new_password" 
                                        name="newPassword"
                                        type="password" 
                                        onChange={this.handleChangePasswords}
                                    />
                                    <label htmlFor="new_password">Nueva contraseña</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col push-l3 l6 s12">
                                    <input 
                                        id="confirm_new_password" 
                                        name="confirmNewPassword"
                                        type="password" 
                                        onChange={this.handleChangePasswords}
                                    />
                                    <label htmlFor="confim_new_password">Confirma nueva contraseña</label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button onClick={this.changePassword} className="waves-effect waves-green btn-flat">Cambiar contraseña</button>
                        <button className=" modal-close waves-effect waves-green btn-flat">Mejor no...</button>
                    </div>
                </div>
            </main>
        )
    }
}
