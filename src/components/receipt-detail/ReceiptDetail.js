import React, { Component } from 'react';
import M from 'materialize-css';
import 'materialize-css';
import style from './receipt-detail.module.css';
import image from '../../assets/images/home-background.jpg';
import { getReceiptDetail } from '../../services/ReceiptsService';
import authHeader from '../../services/auth-header';
import userService from '../../services/user.service';

/**
 * Component to see receipt detail
 *
 * @version 1.0.0
 * @author [Carlos Esteban](https://gitlab.com/iaw47999217)
 */
export default class ReceiptDetail extends Component {

    constructor(props){
        super(props);
        this.state = {
            receipt: null,
            status: null,
            diners: null,
            ingredients: null,
            isLoggedIn:authHeader()
        }
        this.ingredientsIni = {};
    }


    componentDidMount(){
        
        this.scrollTop();
        let id = this.props.id;
        
        //Si existe la prop id hacemos la llamada al servicio
        if (id && id !== null && id !== undefined) {
            this.getReceiptData(id);
        }
        this.initTooltips();
    }

    /**
     * Get the receipt data from the service function
     * @param id id of the receipt
     * @public
     */
    getReceiptData = async(id) => {
        getReceiptDetail(id)
            .then( response => {
                this.ingredientsIni = response.ingredients
                this.setState({
                    receipt: response,
                    status: "success",
                    diners: response.numportions,
                    ingredients: response.ingredients
                }, () => {
                    //Calcula  los ingredientes
                    this.ingredientsQuantityIni(this.state.receipt.numportions);
                })
            })
            .catch(error => {
                M.toast({
                    html:`${error}`,
                    classes:'rounded errorToast'
                })
                this.setState({
                    receipt: null,
                    status: "failed"
                })
            })
    }

    /**
     * Render the level of the receipt
     * @param number Number of difficulty
     * @public
     */
    renderDifficulty = (number) => {
        switch (number) {
            case 1:
                return(
                    <React.Fragment>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad" >star_border</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star_border</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star_border</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star_border</i>
                    </React.Fragment>
                );
                break;
            case 2:
                return(
                    <React.Fragment>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad" >star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star_border</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star_border</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star_border</i>
                    </React.Fragment>
                );
                break;
            case 3:
                return(
                    <React.Fragment>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad" >star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star_border</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star_border</i>
                    </React.Fragment>
                );
                break;
            case 4:
                return(
                    <React.Fragment>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad" >star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star_border</i>
                    </React.Fragment>
                );
                break;
            case 5:
                return(
                    <React.Fragment>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad" >star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star</i>
                        <i className="material-icons tooltipped" data-position="bottom" data-tooltip="Dificultad">star</i>
                    </React.Fragment>
                );
                break;
            default:
                break;
        }
    }

    /**
     * Saves favourite receipt
     * @param
     * @public
     */
    saveFavReceipt = async() => {
        await userService.toogleFavReceipt(this.state.receipt.id)
            .then( response => {
                if (response.data.success) {
                    let receipt = this.state.receipt;
                    receipt.favourite = true;
                    this.setState({
                        receipt: receipt
                    })
                } else {
                    M.toast({
                        html:`${response.data.message}`,
                        classes:'rounded errorToast'
                    })
                }
            })
            .catch(error => {
                M.toast({
                    html:`${error}`,
                    classes:'rounded errorToast'
                })
            })
    }

    /**
     * Undo favourite receipt
     * @param
     * @public
     */
    undoFavReceipt = async() => {
        await userService.toogleFavReceipt(this.state.receipt.id)
            .then(response => {
                if (response.data.success) {
                    let receipt = this.state.receipt;
                    receipt.favourite = false;
                    this.setState({
                        receipt: receipt
                    })
                } else {
                    M.toast({
                        html:`${response.data.message}`,
                        classes:'rounded errorToast'
                    })
                }
            })
            .catch(error => {
                M.toast({
                    html:`${error}`,
                    classes:'rounded errorToast'
                })
            })
    }

    /**
     * Render favButton depending if the receipt is a fav one when the user is LogedIn
     * @param
     * @public
     */
    renderFavButton = () => {
        if (this.state.isLoggedIn !== null) {
            if (this.state.receipt.favourite) {
                return (
                    <button onClick={this.undoFavReceipt} className={`waves-effect waves-light btn ${style.favBtn}`}><i className={`material-icons left ${style.favIcon}`}>favorite</i>receta guardada</button>
                );
            } else {
                return (
                    <button onClick={this.saveFavReceipt} className={`waves-effect waves-light btn ${style.favBtn}`}><i className={`material-icons left ${style.favIcon}`}>favorite_border</i>guardar receta</button>
                );
            }
        }
    }

    /**
     * Scrolls window to the top
     * @public
     */
    scrollTop = () => {
        window.scrollTo({ top: 0 });
    }

    /**
     * This function initialize tooltips of difficult icons
     * @param
     * @public
     */
    initTooltips = () => {
        var options = {}
        var elems = document.querySelectorAll('.tooltipped');
        M.Tooltip.init(elems, options);
    }

    /**
     * Change diners quantity (-1)
     * @param
     * @public
     */
    handleChangeDinerDown = () => {
        let number = this.state.diners - 1;
        //Controlamos que los comensales no bajen del 1
        if (number <= 0) {
            number = 1;
        }
        this.changeIngredientsQuantity(number);
    }

    /**
     * Change Diners quantity (+1)
     * @param
     * @public
     */
    handleChangeDinerUp = () => {
        let number = this.state.diners + 1;
        this.changeIngredientsQuantity(number);
    }

    /**
     * Calculates the initial quantity of ingredients in relation to diner number by default
     * @param number Number of initial quantity of diners
     * @public
     */
    ingredientsQuantityIni = (number) => {
        let ingredients = this.state.ingredients;
        ingredients.forEach((ingredient, i) => {
            ingredient.quantity = this.state.ingredients[i].quantity * number;
        })
        this.setState({
            ingredients: ingredients
        })
    }

    /**
     * Calculates the ingredients quantity in relation to diners number
     * @param number number of diners
     * @public
     */
    changeIngredientsQuantity = (number) => {
        let ingredients = this.state.ingredients;
        
        ingredients.forEach((ingredient, i) => {
            let quantityIni = this.state.ingredients[i].quantity / this.state.diners;
            ingredient.quantity = quantityIni * number;
        })
        this.setState({
            diners: number,
            ingredients: ingredients
        })
    }

    render() {
        if (this.state.receipt !== null && this.state.status === "success") {
            return (
                <main className={style.main}>
                    <div className="container">
                        <div className={`row ${style.mb0}`}>
                            <div className="col s12">
                                <h1 className={`${style.receiptTitle} center`}>{this.state.receipt.name}</h1>
                            </div>
                            <div className="col s12 push-l2 l8 center">
                                {
                                    this.state.receipt.image !== null && this.state.image === undefined ? (
                                        <img className="responsive-img" src={this.state.receipt.image} alt={this.state.receipt.name} />
                                    ) : (
                                        <img className="responsive-img" src={image} alt={this.state.receipt.name} />
                                    )
                                }
                                
                            </div>
                            <div className={`col s12 ${style.timeDiv}`}>
                                <div className={`col s12 l6 ${style.alignRight}`}>
                                {this.renderDifficulty(this.state.receipt.difficulty)}
                                </div>
                                <div className={`col s12 l6 ${style.alignLeft}`}>
                                <i className="tiny material-icons tooltipped" data-position="bottom" data-tooltip="Tiempo de preparación">access_time</i><span className={style.receiptTime}>{this.state.receipt.time}'</span>
                                </div>
                            </div>
                            <div className="col s12 push-l2 l8">
                                <p>{this.state.receipt.description}</p>
                            </div>
                            <div className={`col s12 center `}>
                                <h2 className={style.sectionTitle}>Comensales</h2>
                                {/* numero comensales */}
                                <div className="center">
                                    {
                                        this.state.diners === 1 ? (
                                            <button className={`${style.dinerBtn} ${style.disabledBtn}`} onClick={this.handleChangeDinerDown}><i className="material-icons">keyboard_arrow_left</i></button>
                                        ) : (
                                            <button className={style.dinerBtn} onClick={this.handleChangeDinerDown}><i className="material-icons">keyboard_arrow_left</i></button>
                                        )
                                    }
                                        <span className={style.diner}>{this.state.diners}</span>
                                    <button className={style.dinerBtn} onClick={this.handleChangeDinerUp}><i className="material-icons">keyboard_arrow_right</i></button>
                                </div>
                                
                            </div>
                            <div className={`col s12 push-l2 l8 ${style.ingredientsDiv}`}>
                                <div className="col s12">
                                    <h3 className={style.ingredientTitle}>Ingredientes</h3>
                                </div>
                                <div className={`col s12 ${style.ingredientsDetailDiv}`}>
                                    {/*Muestra los ingredientes*/
                                        this.state.ingredients.map((item, i)=>{
                                            return(
                                                <div key={i} className="col s12 l6 center">
                                                    <div className="col push-s2 s10 left-align">
                                                        <p>{item.ingredient}</p>
                                                        <span>{item.quantity} {item.measure}</span>
                                                    </div>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                            <div className="col s12 center">
                                <h2 className={style.sectionTitle}>Pasos</h2>
                            </div>
                            <div className="col s12">
                                {   /*Muestra los pasos */
                                    this.state.receipt.steps.map((item, i) => {
                                        return(
                                            <React.Fragment key={i}>
                                                <div  className="col s12">
                                                    <div className={`col s1 l2 right-align ${style.stepNumber}`}>{i+1}.</div>
                                                    <div className="col s11 l8">
                                                        <p>{item}</p>
                                                    </div>
                                                </div>
                                                <div className="col s12 push-l2 l8">
                                                    <div className={`divider ${style.dividerLane}`}></div>
                                                </div>
                                            </React.Fragment>
                                        );
                                    })
                                }
                            </div>
                            <div className={`col s12 center ${style.favBtnDiv}`}>
                                {this.renderFavButton()}
                            </div>
                        </div>
                    </div>
                </main>
            )
        } else if (this.state.status === "failed") {
            return (
                <main className={style.main}>
                    <div className={`row ${style.mb0} ${style.fullHeight} center`}>
                        Ha habido un error. Vuelve a intentarlo.
                    </div>
                </main>
            );
        }else{//Si esta cargando
            return (
                <main className={style.main}>
                    <div className={`row ${style.mb0} ${style.fullHeight} center`}>
                        <div className="preloader-wrapper big active ">
                            <div className="spinner-layer spinner-blue ">
                                <div className="circle-clipper left">
                                <div className="circle"></div>
                                </div><div className="gap-patch">
                                <div className="circle"></div>
                                </div><div className="circle-clipper right">
                                <div className="circle"></div>
                                </div>
                            </div>
                            <div className="spinner-layer spinner-red">
                                <div className="circle-clipper left">
                                <div className="circle"></div>
                                </div><div className="gap-patch">
                                <div className="circle"></div>
                                </div><div className="circle-clipper right">
                                <div className="circle"></div>
                                </div>
                            </div>
                            <div className="spinner-layer spinner-yellow">
                                <div className="circle-clipper left">
                                <div className="circle"></div>
                                </div><div className="gap-patch">
                                <div className="circle"></div>
                                </div><div className="circle-clipper right">
                                <div className="circle"></div>
                                </div>
                            </div>
                            <div className="spinner-layer spinner-green">
                                <div className="circle-clipper left">
                                <div className="circle"></div>
                                </div><div className="gap-patch">
                                <div className="circle"></div>
                                </div><div className="circle-clipper right">
                                <div className="circle"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            );
        }
    }
}
