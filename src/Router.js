import React, { Component } from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import authHeader from './services/auth-header';

//Component imports
import Login from './components/auth/login/Login';
import Registration from './components/auth/registration/Registration';
import Home from './components/home/Home';
import Header from './components/commons/header/Header';
import SearchForm from './components/search-form/SearchForm';
import FoundedReceipts from './components/founded-receipts/FoundedReceipts';
import ReceiptDetail from './components/receipt-detail/ReceiptDetail';
import UserDetail from './components/user-detail/UserDetail';
import PrimarySearch from './components/primary-search/PrimarySearch';
import EditSearch from './components/edit-search/EditSearch';


const PrivateRoute = ({component: Component,...rest}) => (
    <Route {...rest} render={(props) => (
        authHeader() !== null ? (
            <React.Fragment>
                <Header />
                <Component {...props} />
            </React.Fragment>
        ) : (
            <Redirect to="/buscar" />
        )
        
    )}/>
);

export default class Router extends Component{
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    {/*Add here all the routes*/}
                    <Route exact path="/" component={Home} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/registro" component={Registration} />
                    <Route exact path="/buscar" render = {() => 
                        <React.Fragment>
                            <Header />
                            <SearchForm />
                        </React.Fragment>
                    } />
                    <Route exact path="/redirect/:search" render= {
                        (props) => {
                            var search = props.match.params.search;
                            return(
                                <Redirect to={'/recetas-encontradas/' + search} />
                            );
                        }
                    } />
                    <Route exact path="/recetas-encontradas/:search/:isSearchSaved?" render = {(props) => {
                            var search = props.match.params.search;
                            var isSearchSaved = props.match.params.isSearchSaved;
                            return(
                                <React.Fragment>
                                    <Header />
                                    <FoundedReceipts search={search} isSearchSaved={isSearchSaved} />
                                </React.Fragment>
                            );
                        }
                    } />
                    <Route exact path="/receta/:id" render = {(props) => {
                            var id = props.match.params.id;
                            return(
                                <React.Fragment>
                                    <Header />
                                    <ReceiptDetail id={id} />
                                </React.Fragment>
                            );
                        }
                    } />
                    <PrivateRoute  path="/perfil" component={UserDetail}/>
                    <PrivateRoute  path="/busqueda-primaria" component={PrimarySearch}/>
                    <PrivateRoute  path="/editar-busqueda/:id" component={EditSearch}/>
                    <Route render = {() =>
                        <React.Fragment>
                            <Header />
                            <SearchForm />
                        </React.Fragment>
                    } />
                </Switch>
            </BrowserRouter>
        )
    }
}
