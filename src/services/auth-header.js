import authService from "./auth.service";

/**
 * Check if the user is logged in
 * @param
 */
export default function authHeader() {
    const token = authService.getCurrentUserToken();
    if (token) {
        return { Authorization: token }
    } else {
        return null;
    }
}