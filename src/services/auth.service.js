import axios from "axios";
import { API_URL, LOCAL_STORAGE_NAME } from "./globals";



class AuthService {
    /**
     * Makes the login request to API
     * @param {*} userEmail 
     * @param {*} password 
     */
    async login(userEmail, password) {
        //Working
        return axios
            .post(API_URL + "/login", {
                email: userEmail,
                password,
                getToken: true
            })
            .then(response => {
                if (response.data.success) {
                    const token = response.data.token;
                    //const user = response.data.user;
                    localStorage.setItem(LOCAL_STORAGE_NAME, token);
                    return response.data;
                } else {
                    // Error de autenticación
                    throw Error(response.data.message);
                }
            });
    }

    /**
     * Makes the logout removing the token from localStorage
     * @param
     */
    logout() {
        //Working
        localStorage.removeItem(LOCAL_STORAGE_NAME);
        window.location.reload();
    }

    /**
     * Makes the register request to API
     * @param {*} username 
     * @param {*} email 
     * @param {*} password 
     * @param {*} password_confirmation 
     */
    register(username, email, password, password_confirmation) {
        //Working
        return axios
            .post(API_URL + "/register", {
                name: username,
                email: email,
                password: password,
                password_confirmation: password_confirmation
            });
    }

    /**
     * Gets the actual session token
     * @param
     */
    getCurrentUserToken() {
        //Working
        return localStorage.getItem(LOCAL_STORAGE_NAME);
    }
}

export default new AuthService();