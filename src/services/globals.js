//API URL
export const API_URL = "http://localhost:8000/api";

//Max searches users can save
export const MAX_SAVED_SEARCHES = 3;

//Name of the localstorage token key
export const LOCAL_STORAGE_NAME = "feedme";

/**
 * Function that transforms api response objects to chips object format
 * @param {*} apiFormatObj Object with api response format
 */
export const apiResponseToChipsFormat = (apiFormatObj) => {
    let ingredients = {};
    for (let i = 0; i < apiFormatObj.length; i++) {
        ingredients[apiFormatObj[i].name] = null;
    }
    return ingredients;
}

/**
 * Functions that transforms chips object to api request format
 * @param {*} chipsFormatObj Object with chips format
 * @param {*} ingredientsApiFormatObj Object with api response format
 * @param {*} allergensApiFormatObj Object with api response format
 */
export const chipsFormatToApiRequest = (chipsFormatObj, ingredientsApiFormatObj, allergensApiFormatObj, isPrimarySearch = false) => {
    let allergensArray = extractIdFromCollection(chipsFormatObj, allergensApiFormatObj, 'allergens');
    let excludedIngredientsArray = extractIdFromCollection(chipsFormatObj, ingredientsApiFormatObj, 'withoutTheseIngredients');;
    let includedIngredientsArray = isPrimarySearch ? [] : extractIdFromCollection(chipsFormatObj, ingredientsApiFormatObj, 'ingredients');
    let transformedSearch = {
        excludedIngredients: excludedIngredientsArray,
        isvegetarian: chipsFormatObj.isvegetarian,
        isvegan: chipsFormatObj.isvegan,
        allergens: allergensArray,
    }

    // Una búsqueda primaria solamente contiene los campos anteriores. En caso de que no lo sea, le añadiremos éstos.
    if (!isPrimarySearch) {
        transformedSearch.title = chipsFormatObj.title;
        transformedSearch.includedIngredients = includedIngredientsArray;
        transformedSearch.maxtime = chipsFormatObj.minutes;
        transformedSearch.tags = [];
    }

    return transformedSearch;
}

/**
 * Extrae los IDs de una colección de elementos en formato chip.
 * 
 * @param {*} chipsFormatObj 
 * @param {*} apiFormatObj 
 * @param {*} collectionName 
 */
const extractIdFromCollection = (chipsFormatObj, apiFormatObj, collectionName) => {
    let collection = [];
    for (let i = 0; i < chipsFormatObj[collectionName].length; i++) {
        for (let j = 0; j < apiFormatObj.length; j++) {
            if (chipsFormatObj[collectionName][i].tag === apiFormatObj[j].name) {
                collection.push(apiFormatObj[j].id);
            }
        }
    }
    return collection;
};

/**
 * This function transforms an object list to an object. The new object is used to autocomplete chips on forms
 * @param {*} list 
 */
export const chipsLoadFormat = (list) => {
    return list.map((element) => {
        return {
            tag: element.name
        };
    })
}

/**
 * Transforms an API object list to an array of id(numbers)
 * @param {*} list 
 */
export const getIdFromApiFormatObj = (list) => {
    return list.map((element) => {
        return element.id;
    })
}