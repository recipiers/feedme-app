import Axios from "axios";
import { API_URL } from "./globals";

/**
 * Function that provides a list of ingredients
 */
export const getIngredients = async() => {
    //Working
    return Axios.get(API_URL + "/ingredients")
        .then(response => response.data.data);
};

/**
 * Function that provides a list of allergens
 * @param
 */
export const getAllergens = async() => {
    //Working
    return Axios.get(API_URL + "/allergens")
        .then(response => response.data);
};

/**
 * Function that makes the receipts search request to API
 * @param search search object to make receipts request 
 */
export const makeReceiptsRequest = async(search) => {
    //Work
    const request = JSON.parse(search);
    return Axios.post(API_URL + "/recipes/search", request);
}