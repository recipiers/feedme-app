import { API_URL } from "./globals";
import Axios from "axios";

/**
 * Function that makes the receipt detail request to API
 * @param id Is the id of the receipt
 */
export const getReceiptDetail = async(id) => {
    //Working
    return Axios.get(API_URL + "/recipes/" + id).then(response => {
        const recipe = response.data.recipe;
        recipe.ingredients = recipe.ingredients.map(i => {
            return {
                ingredient: i.ingredient.name,
                quantity: parseInt(i.amount),
                measure: i.unit.name
            };
        });
        recipe.image = recipe.pictureurl;
        return recipe;
    });
}