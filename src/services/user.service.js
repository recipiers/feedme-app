import Axios from "axios";
import authHeader from './auth-header';
import M from 'materialize-css';
import { API_URL, chipsLoadFormat } from "./globals";


class UserService {
    /**
     * Get the actual user data
     */
    async getUserProfileData() {
        //Working
        return Axios.get(API_URL + "/user/me", { headers: authHeader() });
    }

    /**
     * Request the user's favorite recipes
     * @param
     */
    async getFavReceipts() {
        //Working
        return Axios.get(API_URL + "/user/favrecipes", { headers: authHeader() })
            .then(response => response.data.fav_recipes)
            .then(favRecipes => {
                return favRecipes.map(recipe => {
                    recipe.favourite = true;
                    return recipe;
                })
            })
    }

    /**
     * Save a favorite recipe
     * @param {*} id 
     */
    async toogleFavReceipt(id) {
        //Working
        return Axios.post(API_URL + "/user/favrecipes/" + id, {}, { headers: authHeader() });
    }

    /**
     * Changes the actual password of the user
     * @param {*} actualPassword 
     * @param {*} newPassword 
     */
    async changePassword(actualPassword, newPassword, confirmNewPassword) {
        //Working
        return Axios.put(API_URL + "/user/changepass", {
            oldpassword: actualPassword,
            oldpassword_confirmation: actualPassword,
            newpassword: newPassword,
            newpassword_confirmation: confirmNewPassword
        }, { headers: authHeader() });
    }

    /**
     * Gets the user primary search
     * @param
     */
    async getPrimarySearch() {
        //Working
        return Axios.get(API_URL + "/user/primarySearch", { headers: authHeader() })
            .then(response => response.data.primarySearch)
            .then(apiPrimarySearch => {
                apiPrimarySearch.doNotHaveTheseIngredients = chipsLoadFormat(apiPrimarySearch.excludedIngredients);
                apiPrimarySearch.allergens = chipsLoadFormat(apiPrimarySearch.allergens);
                return apiPrimarySearch;
            });
    }

    /**
     * Saves the user primary search
     * @param {*} primarySearch 
     */
    async updatePrimarySearch(primarySearch) {
        // Working
        return Axios.put(API_URL + "/user/primarySearch", primarySearch, {
            headers: authHeader()
        });
    }

    /**
     * Save a search of the actual user
     * @param {*} search 
     */
    async saveSearch(title, search) {
        //Working
        return Axios.post(API_URL + "/user/storedSearches", {
            title: title,
            isvegetarian: search.isvegetarian,
            isvegan: search.isvegan,
            maxtime: search.maxtime,
            includedIngredients: search.includedIngredients,
            excludedIngredients: search.excludedIngredients,
            allergens: search.allergens,
            includedTags: []
        }, { headers: authHeader() })
    }

    /**
     * Makes the request to get the saved searches of the actual user
     * @param
     */
    async getSavedSearches() {
        //Working
        return Axios.get(API_URL + "/user/storedSearches", { headers: authHeader() })
            .then(response => response.data.storedSearches);
    }

    async getStoredSearch(idSearch) {
        //Working
        return Axios.get(API_URL + "/user/storedSearches/" + idSearch, { headers: authHeader() })
            .then(response => response.data.search)
            .then(searchData => {
                searchData.allergensOrig = searchData.allergens;
                searchData.haveThisIngredients = chipsLoadFormat(searchData.includedIngredients);
                searchData.doNotHaveTheseIngredients = chipsLoadFormat(searchData.excludedIngredients);
                searchData.allergens = chipsLoadFormat(searchData.allergens);
                searchData.maxtime = searchData.maxtime.toString();
                return searchData;
            })
    }

    /**
     * Updates a saved serch
     * @param {*} idSearch 
     * @param {*} search 
     */
    async updateSearch(idSearch, search) {
        //Working
        return Axios.put(API_URL + "/user/storedSearches/" + idSearch, search, { headers: authHeader() });
    }

    /**
     * Deletes a saved search
     * @param {*} idSearch 
     */
    async deleteSearch(idSearch) {
        //Working
        Axios.delete(API_URL + "/user/storedSearches/" + idSearch, { headers: authHeader() })
            .then(response => {
                M.toast({
                    html: `La búsqueda se ha eliminado correctamente`,
                    classes: 'rounded succedToast'
                })
            })
            .catch(error => {
                M.toast({
                    html: `La búsqueda no ha podido eliminarse, intentalo más tarde. ${error}`,
                    classes: 'rounded errorToast'
                })
            })
    }

    /**
     * Delete actual user account
     * @param
     */
    async deleteAccount() {
        //Working
        return Axios.delete(API_URL + "/user", { headers: authHeader() });
    }
}
export default new UserService();